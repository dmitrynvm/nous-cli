plugins {
    kotlin("jvm") version "1.6.0"
    kotlin("plugin.serialization") version "1.4.20"
    application
}

dependencies {
    implementation("com.github.ajalt.clikt:clikt:3.4.2")
    implementation("com.google.guava:guava:31.1-jre")
    implementation("com.google.code.gson:gson:2.9.0")
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.1")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.10")
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation("me.tongfei:progressbar:0.9.3")
    implementation("de.vandermeer:asciitable:0.3.2")
    implementation("commons-codec:commons-codec:1.15")
    implementation("commons-io:commons-io:2.11.0")
    implementation("com.hubspot.jinjava:jinjava:2.6.0")
    implementation("org.slf4j:slf4j-simple:1.7.36")
    implementation("org.jetbrains.spek:spek-api:1.1.5")
    implementation("com.github.oshi:oshi-core:6.1.6")
    implementation("com.sksamuel.hoplite:hoplite-core:2.1.2")
    implementation("com.sksamuel.hoplite:hoplite-yaml:2.1.2")
    implementation("io.github.serpro69:kotlin-faker:1.10.0")
    implementation("com.esotericsoftware.yamlbeans:yamlbeans:1.15")
    implementation("org.litote.kmongo:kmongo:4.6.0")

    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
}

repositories {
    mavenCentral()
}

distributions {
    main {
        contents {
            from("src/main/resources") {
                include("**/*.yml")
            }
        }
    }
}

tasks.distTar {
    enabled = false
}

tasks.withType<Test> {
    minHeapSize = "2048m"
    maxHeapSize = "8064m"
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}

buildscript {
    repositories {
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
    }
}

application {
    mainClass.set("nous.application.MainKt")
}
