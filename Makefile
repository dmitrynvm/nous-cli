configure:
	gradle wrapper

build: configure
	gradle build --refresh-dependencies

test:
	gradle test

install:
	rm -rf ~/.nous; unzip build/distributions/nous.zip -d ~/

clean:
	rm -rf build gradle gradlew*

publish:
	scp build/distributions/nous*.zip ${name}@188.124.50.201:/home/${name}
