# Nous-CLI Data Pipeline Automation Tools

0. Prerequisites:  JDK(11-17), Kotlin (>1.6.0), Gradle (>1.7.0)

1. Clone the repository

```bash
git clone https://git.nous.dev/brain/nous-cli
```

2. Wrap and build the project with gradle

```bash
make configure
make build
```

3. Install the nous-cli and add the install path to system variables

```bash
make install
```

4. Initialize and ensure that is works well

```bash
nous system info
nous init
```

5. List and load the dataset contents for future analysis

```bash
nous ls <path-to-data>
nous load <path-to-data>
```

## Code style and analyzing

1. Please follow next coding convention - https://kotlinlang.org/docs/coding-conventions.html

2. `Ktlint Gradle` plugin is used to provide ktlint linter abilities - https://github.com/jlleitschuh/ktlint-gradle. To
   perform sources check run `ktlintCheck` task, to perform all possible formatting on sources run `ktlintFormat` task.
   To see all available tasks please check git-hub project repo.

3. `Detekt` plugin is used to provide static code analysis abilities - https://github.com/detekt/detekt. To perform
   sources analyzing run `detekt` task. To see all available tasks please check git-hub project repo.

4. `Jacoco` plugin is used to provide code coverage metrics
    - https://docs.gradle.org/current/userguide/jacoco_plugin.html. To create code coverage report
      run `jacocoTestReport`
      task.

## Development strategy

It was agreed to use Trunk Based Development (TBD) as the development strategy. Please read and follow main concerns of
this strategy on https://trunkbaseddevelopment.com

## Git branching and commits recommendations

1. Supported branch name prefixes:
    - `feature/feat` - feature associated changes and updates
    - `hotfix/fix` - bug fixes branch
2. Branch name structure '<branch_name_prefix>/<jira_task_id(in case of hotfix it could be optional)>-<
   short_description>'
   Example: `feature/BRN-202_add-code-analyze-plugins`
3. Commit header should start with JIRA task id (in case of hotfix it could be optional) and short commit description.
   For consistency, try and use the imperative present tense when creating a message (50 chars or fewer).
   Example `BRN-202 Add code style and analyze plugins`
4. Please add more detailed explanatory text, if necessary. Wrap it to about 72 characters or so, it could be present as
   bullet points as well. Example:
    - Add ktlint code style plugin
    - Add detekt static code analysis plugin
    - Add jacoco code coverage plugin
    - Update build configs
    - Update README.md
      `



