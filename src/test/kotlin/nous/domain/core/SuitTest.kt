package nous.domain.core

import kotlin.test.*

class SuitTest {
    @Test fun `All elements do exist`() {
        assertEquals(Suit.CLUBS.toInt(), 0)
        assertEquals(Suit.DIAMONDS.toInt(), 1)
        assertEquals(Suit.HEARTS.toInt(), 2)
        assertEquals(Suit.SPADES.toInt(), 3)
    }

    @Test fun `Fabric methods do work`() {
        assertNotNull(Suit.all().toList().size)
        assertNotNull(Suit.any())
        assertEquals(Suit.HEARTS, Suit(2))
        assertEquals(Suit.DIAMONDS, Suit("d"))
        assertEquals(Rank.NONE, Rank("X"))
        assertEquals(Rank.ACE.toString(), "A")
        assertEquals(Rank.NINE.toInt(), 7)
    }
}
