package nous.domain.core

import kotlin.test.Test
import kotlin.test.*

class CardTest {
    @Test fun `Fabric methods do work` () {
        assertNotNull(Card.all().toList().size)
        assertNotNull(Card.any(1))
        assertEquals(Card("As"), Card(Rank.ACE, Suit.SPADES))
        assertEquals(Card("As").rank, Rank.ACE)
        assertEquals(Card("As").suit, Suit.SPADES)
    }
}
