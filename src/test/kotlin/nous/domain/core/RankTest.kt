package nous.domain.core

import kotlin.test.*

class RankTest {
    @Test fun `All elements do exist`() {
        assertEquals(Rank.TWO.toInt(), 0)
        assertEquals(Rank.THREE.toInt(), 1)
        assertEquals(Rank.FOUR.toInt(), 2)
        assertEquals(Rank.FIVE.toInt(), 3)
        assertEquals(Rank.SIX.toInt(), 4)
        assertEquals(Rank.SEVEN.toInt(), 5)
        assertEquals(Rank.EIGHT.toInt(), 6)
        assertEquals(Rank.NINE.toInt(), 7)
        assertEquals(Rank.TEN.toInt(), 8)
        assertEquals(Rank.JACK.toInt(), 9)
        assertEquals(Rank.QUEEN.toInt(), 10)
        assertEquals(Rank.KING.toInt(), 11)
        assertEquals(Rank.ACE.toInt(), 12)
    }

    @Test fun `Fabric methods do work`() {
        assertNotNull(Rank.all().toList().size)
        assertNotNull(Rank.one())
        assertEquals(Rank.KING, Rank("K"))
        assertEquals(Rank.NONE, Rank("X"))
        assertEquals(Rank.ACE.toString(), "A")
        assertEquals(Rank.NINE.toInt(), 7)
    }
}