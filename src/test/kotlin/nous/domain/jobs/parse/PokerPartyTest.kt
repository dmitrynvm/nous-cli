package nous.domain.jobs.parse

import nous.domain.core.*
import kotlin.test.*

class PokerPartyTest {

    @Test fun `PartyPoker hands parser`() {
//        println(parse(input.lines()).join())
//        assertTrue(parse(input.lines()).isRight())
    }

    @Test fun `PartyPoker file parser`() {
//        for(hand in path.open().read().hands())
//            println(parse(hand.lines()).join())
//        assertTrue(path.open().read().hands().all { parse(it.lines()).isRight() })
    }

    private val parse = Parse(Room.POKERPARTY)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokerparty.txt"
    private val input = """
        ***** Hand History For Game 1646228986177boe3sicfvht *****
        0.10/0.25 Texas Holdem Game Table (NL) - Wed Mar 02 08:48:30 EST 2022
        Table Table 6914850 (Real Money) -- Seat 1 is the button
        Total number of players : 6/6
        Seat 1: Player6 ($15.97)
        Seat 2: Player1 ($24.01)
        Seat 3: Player2 ($26.73)
        Seat 4: Player3 ($25)
        Seat 5: Hero ($25)
        Seat 6: Player5 ($31.01)
        Player1 posts small blind (0.10)
        Player2 posts big blind (0.25)
        ** Dealing down cards **
        Dealt to Hero [ Ac, 9c ]
        Player3 folds 
        Hero raises 0.60 to 0.60
        Player5 calls (0.60)
        Player6 folds 
        Player1 calls (0.50)
        Player2 folds 
        ** Dealing Flop ** :  [ Qs, 4h, 3s ]
        Player1 checks 
        Hero checks 
        Player5 bets (1.17)
        Player1 calls (1.17)
        Hero folds 
        ** Dealing Turn ** :  [ Kh ]
        Player1 checks 
        Player5 bets (2.50)
        Player1 raises 8.62 to 8.62
        Player5 calls (6.12)
        Creating Main Pot with $ 20.55 with Player1
        ** Dealing River ** :  [ 4d ]
        Player1 bets (13.62)
        Player1 is all-In.
        Player5 folds 
        ** Summary **
        Main Pot: $20.55 Rake: $1.08
        Board: [ Qs, 4h, 3s, Kh, 4d ]
        Player6 balance $15.97, didn't bet (folded)
        Player1 balance $34.17, bet $24.01, collected $34.17, net +$10.16
        Player2 balance $26.48, lost $0.25 (folded)
        Player3 balance $25, didn't bet (folded)
        Hero balance $24.40, lost $0.60 (folded)
        Player5 balance $20.62, lost $10.39 (folded)
    """.trimIndent()
}
