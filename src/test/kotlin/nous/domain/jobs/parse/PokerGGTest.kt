package nous.domain.jobs.parse

import nous.domain.core.*
import kotlin.test.*

class PokerGGTest {

    @Test fun `PokerGG hands parser`() {
//        println(parse(input.lines()).join())
//        assertTrue(parse(input.lines()).isRight())
    }

    @Test fun `PokerGG file parser`() {
//        for(hand in path.open().read().hands())
//            println(parse(hand.lines()).join())
//        assertTrue(path.open().read().hands().all { parse(it.lines()).isRight() })
    }

    private val parse = Parse(Room.POKERGG)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokergg.txt"
    private val input = """
        Poker Hand #RC739474138: Hold'em No Limit  ($0.01/$0.02) - 2022/03/02 15:42:38
        Table 'RushAndCash1607777' 6-max Seat #1 is the button
        Seat 1: 76fc45d8 ($2.53 in chips)
        Seat 2: Hero ($4.28 in chips)
        Seat 3: 8ed6c7e3 ($3.13 in chips)
        Seat 4: 332aafe1 ($3.21 in chips)
        Seat 5: d77d23af ($4.98 in chips)
        Seat 6: aa23fe5b ($1.84 in chips)
        Hero: posts small blind $0.01
        8ed6c7e3: posts big blind $0.02
        *** HOLE CARDS ***
        Dealt to 76fc45d8 
        Dealt to Hero [9c 8h]
        Dealt to 8ed6c7e3 
        Dealt to 332aafe1 
        Dealt to d77d23af 
        Dealt to aa23fe5b 
        332aafe1: raises $0.04 to $0.06
        d77d23af: folds
        aa23fe5b: raises $0.06 to $0.12
        76fc45d8: calls $0.12
        Hero: folds
        8ed6c7e3: calls $0.1
        332aafe1: calls $0.06
        *** FLOP *** [Kd Jd Js]
        8ed6c7e3: checks
        332aafe1: checks
        aa23fe5b: checks
        76fc45d8: checks
        *** TURN *** [Kd Jd Js] [6s]
        8ed6c7e3: checks
        332aafe1: bets $0.28
        aa23fe5b: folds
        76fc45d8: folds
        8ed6c7e3: folds
        Uncalled bet ($0.28) returned to 332aafe1
        *** SHOWDOWN ***
        332aafe1 collected $0.47 from pot
        *** SUMMARY ***
        Total pot $0.49 | Rake $0.02 | Jackpot $0 | Bingo $0
        Board [Kd Jd Js 6s]
        Seat 1: 76fc45d8 (button) folded on the Turn
        Seat 2: Hero (small blind) folded before Flop
        Seat 3: 8ed6c7e3 (big blind) folded on the Turn
        Seat 4: 332aafe1 won ($0.47)
        Seat 5: d77d23af folded before Flop (didn't bet)
        Seat 6: aa23fe5b folded on the Turn
    """.trimIndent()
}
