package nous.domain.jobs.parse

import nous.domain.core.*
import nous.utils.io.*
import kotlin.test.*

class Poker888Test {
    @Test fun `Poker888 hand parser`() {
//        assertTrue(parse(clean(input1.lines())).isRight())
//        assertTrue(parse(clean(input2.lines())).isRight())
//        assertTrue(parse(clean(input3.lines())).isRight())
//        assertTrue(parse(clean(input4.lines())).isRight())
//        assertTrue(parse(clean(input5.lines())).isRight())
//        assertTrue(parse(clean(input6.lines())).isRight())
//        assertTrue(parse(clean(input7.lines())).isRight())
    }

    @Test fun `Party888 file parser`() {
//        assertTrue(path.open().read().hands().all { parse(clean(it.lines())).isRight() })
    }

//    private val rotate = Rotate()
    private val parse = Parse(Room.POKER888)
    private val path = "src/test/resources/nous/domain/jobs/parse/poker888.txt"
    private val input1 =
    """
        ***** 888poker Hand History for Game 441802597 *****
        $2/$4 Blinds No Limit Holdem - *** 25 12 2017 01:06:33
        Table Colombes 6 Max (Real Money)
        Seat 7 is the button
        Total number of players : 6
        Seat 1: Rallyeenno ( $192 )
        Seat 2: maybeshine ( $400 )
        Seat 4: Uskebasi_1 ( $400 )
        Seat 6: xMiguelitox ( $400 )
        Seat 7: BustoKid ( $448.10 )
        Seat 9: PridvorD ( $439.60 )
        PridvorD posts small blind [$2]
        Rallyeenno posts big blind [$4]
        ** Dealing down cards **
        Dealt to Uskebasi_1 [ 3s, Kh ]
        maybeshine raises [$12]
        Uskebasi_1 folds
        xMiguelitox calls [$12]
        BustoKid folds
        PridvorD folds
        Rallyeenno folds
        ** Dealing flop ** [ 8h, 2d, 4h ]
        maybeshine bets [$18]
        xMiguelitox calls [$18]
        ** Dealing turn ** [ Ah ]
        maybeshine checks
        xMiguelitox bets [$49.50]
        maybeshine calls [$49.50]
        ** Dealing river ** [ Ts ]
        maybeshine checks
        xMiguelitox bets [$119.75]
        maybeshine calls [$119.75]
        ** Summary **
        xMiguelitox shows [ Jh, Th ]
        maybeshine mucks [ Ac, Td ]
        xMiguelitox collected [ $400.50 ]
    """.trimIndent()
    private val input2 =
    """
        ***** 888poker Hand History for Game 276577542 *****
        $0.02/$0.04 Blinds No Limit Holdem - *** 20 12 2013 21:44:14
        Table Bolzano 6 Max (Real Money)
        Seat 6 is the button
        Total number of players : 6
        Seat 1: Uskebasi_1 ( $5.31 )
        Seat 2: Sizikov ( $4.26 )
        Seat 4: seljak11 ( $10.55 )
        Seat 6: alano97 ( $2.59 )
        Seat 7: Cinquin ( $2 )
        Seat 9: RomanKask ( $4.40 )
        Cinquin posts small blind [$0.02]
        RomanKask posts big blind [$0.04]
        ** Dealing down cards **
        Dealt to Uskebasi_1 [ 3c, 4s ]
        Uskebasi_1 folds
        Sizikov raises [$0.12]
        seljak11 folds
        alano97 folds
        Cinquin folds
        RomanKask folds
        ** Summary **
        Sizikov did not show his hand
        Sizikov collected [ $0.10 ]
    """.trimIndent()
    private val input3 =
    """
        ***** 888poker Hand History for Game 503593650 *****
        $1/$2 Blinds No Limit Omaha - *** 25 04 2015 03:09:08
        Table Junin 6 Max (Real Money)
        Seat 1 is the button
        Total number of players : 4
        Seat 1: andrew2912 ( $110.14 )
        Seat 2: Uskebasi_1 ( $10 )
        Seat 4: bugr1231 ( $8 )
        Seat 6: YYYYvsMoi ( $240.63 )
        Uskebasi_1 posts small blind [$1]
        bugr1231 posts big blind [$2]
        YYYYvsMoi calls [$2]
        andrew2912 calls [$2]
        Uskebasi_1 raises [$9]
        bugr1231 calls [$6]
        YYYYvsMoi calls [$8]
        andrew2912 calls [$8]
        ** Dealing flop ** [ 7h, 3h, Qs ]
        YYYYvsMoi checks
        andrew2912 checks
        ** Dealing turn ** [ 4s ]
        YYYYvsMoi checks
        andrew2912 checks
        ** Dealing river ** [ 6h ]
        YYYYvsMoi checks
        andrew2912 checks
        ** Summary **
        YYYYvsMoi shows [ 9c, 6d, Ac, 9h ]
        andrew2912 shows [ Jd, 6s, Tc, Jh ]
        Uskebasi_1 shows [ 4c, 3c, 7d, 5d ]
        bugr1231 mucks [ 7c, 3s, 9d, Jc ]
        Uskebasi_1 collected [ $30.94 ]
        Uskebasi_1 collected [ $5.80 ]
    """.trimIndent()
    private val input4 =
    """
           ***** 888poker Hand History for Game 1164913283 *****
        $0.50/$1 Blinds No Limit Holdem Jackpot table - *** 20 04 2018 23:43:32
        Table Upington 10 Max (Real Money)
        Seat 6 is the button
        Total number of players : 10
        Seat 1: balics123 ( $44.21 )
        Seat 2: Rabestro ( $218.58 )
        Seat 3: zvrc011 ( $106.82 )
        Seat 4: proker29 ( $100 )
        Seat 5: vanapeer ( $229.64 )
        Seat 6: DigNuS8DuRuM ( $100 )
        Seat 7: Uskebasi_1 ( $100 )
        Seat 8: SCAARLETTT97 ( $101.50 )
        Seat 9: Jody_Jody ( $178.85 )
        Seat 10: ptrckbateman ( $139.26 )
        Uskebasi_1 posts small blind [$0.50]
        SCAARLETTT97 posts big blind [$1]
        ** Dealing down cards **
        Dealt to Uskebasi_1 [ 6d, Qs ]
        Jody_Jody folds
        ptrckbateman folds
        balics123 raises [$3.50]
        Rabestro folds
        zvrc011 folds
        proker29 raises [$12]
        vanapeer folds
        DigNuS8DuRuM folds
        Uskebasi_1 folds
        SCAARLETTT97 folds
        balics123 calls [$8.50]
        ** Dealing flop ** [ 6c, 2s, Qd ]
        balics123 checks
        proker29 bets [$6]
        balics123 folds
        ** Summary **
        proker29 did not show his hand
        proker29 collected [ $24.23 ]
    """.trimIndent()
    private val input5 =
    """
        ***** 888.es Hand History for Game 411096474 *****
        0,25 €/0,50 € Blinds No Limit Holdem - *** 22 10 2020 03:51:43
        Table Benamargosa 6 Max (Real Money)
        Seat 7 is the button
        Total number of players : 6
        Seat 1: Pogreb1 ( 50 € )
        Seat 2: Shedas ( 53,44 € )
        Seat 4: RickandRoll ( 42,32 € )
        Seat 6: tightisrigh1 ( 79,72 € )
        Seat 7: M25s22j18 ( 28,32 € )
        Seat 9: tiltstinc ( 67,74 € )
        tiltstinc posts small blind [0,25 €]
        Pogreb1 posts big blind [0,50 €]
        ** Dealing down cards **
        Dealt to Pogreb1 [ Qh, 6s ]
        Shedas folds
        RickandRoll raises [1,25 €]
        tightisrigh1 folds
        M25s22j18 calls [1,25 €]
        tiltstinc folds
        Pogreb1 folds
        ** Dealing flop ** [ 2s, Kc, 9d ]
        RickandRoll checks
        M25s22j18 bets [0,50 €]
        RickandRoll calls [0,50 €]
        ** Dealing turn ** [ Ad ]
        RickandRoll checks
        M25s22j18 bets [0,50 €]
        RickandRoll folds
        ** Summary **
        M25s22j18 did not show his hand
        M25s22j18 collected [ 4,04 € ]
    """.trimIndent()

    private val input6 =
    """
        ***** 888poker Hand History for Game 1191023792 *****
        $0.50/$1 Blinds No Limit Holdem Jackpot table - *** 08 07 2018 02:36:48
        Table Helmond 10 Max (Real Money)
        Seat 4 is the button
        Total number of players : 10
        Seat 1: zvrc011 ( $100 )
        Seat 2: UEmtekin ( $110.70 )
        Seat 3: Uskebasi_1 ( $103.37 )
        Seat 4: tupacalips ( $167.25 )
        Seat 5: DayMan. ( $95.88 )
        Seat 6: _light3bet_ ( $100 )
        Seat 7: SS20S ( $74.77 )
        Seat 8: Pokeroidz ( $111.95 )
        Seat 9: UnluckyMeGG ( $100 )
        Seat 10: baldeagle25 ( $82.11 )
        DayMan. posts small blind [$0.50]
        _light3bet_ posts big blind [$1]
        ** Dealing down cards **
        Dealt to Uskebasi_1 [ 2h, Kd ]
        SS20S folds
        Pokeroidz calls [$1]
        UnluckyMeGG folds
        baldeagle25 calls [$1]
        zvrc011 folds
        UEmtekin folds
        Uskebasi_1 folds
        tupacalips folds
        DayMan. calls [$0.50]
        _light3bet_ checks
        ** Dealing flop ** [ 9h, As, Td ]
        DayMan. checks
        _light3bet_ checks
        Pokeroidz checks
        baldeagle25 checks
        ** Dealing turn ** [ Qs ]
        DayMan. checks
        _light3bet_ checks
        Pokeroidz checks
        baldeagle25 checks
        ** Dealing river ** [ 3d ]
        DayMan. checks
        _light3bet_ checks
        Pokeroidz bets [$2.66]
        baldeagle25 folds
        DayMan. folds
        _light3bet_ folds
        ** Summary **
        Pokeroidz did not show his hand
        Pokeroidz collected [ $3.80 ]
    """.trimIndent()
    private val input7 =
    """
        ***** 888.es Snap Poker Hand History for Game 291587532 *****
        0,25 €/0,50 € Blinds No Limit Holdem - *** 01 10 2020 23:24:41
        Table Sobradiel 6 Max (Real Money)
        Seat 1 is the button
        Total number of players : 5
        Seat 1: DegoladosXX ( 22,63 € )
        Seat 2: Lolain25 ( 52,07 € )
        Seat 4: Pogreb1 ( 117,45 € )
        Seat 6: Manchego4651 ( 20,44 € )
        Seat 7: Suzuki_Gun ( 57,87 € )
        Lolain25 posts small blind [0,25 €]
        Pogreb1 posts big blind [0,50 €]
        ** Dealing down cards **
        Dealt to Pogreb1 [ Qs, Kd ]
        Manchego4651 folds
        Suzuki_Gun folds
        DegoladosXX folds
        Lolain25 folds
        ** Summary **
        Pogreb1 did not show his hand
        Pogreb1 collected [ 0,75 € ]
    """.trimIndent()
}
