package nous.domain.jobs.parse

import nous.domain.core.*
import nous.utils.io.*
import nous.utils.fmt.*
import kotlin.test.*

class PokerStarsTest {

    @Test fun `PokerStars hands parser`() {
//        println(parse(input.lines()))
//        println(parse(input.lines()).join())
//        assertTrue(parse(input.lines()).isRight())
    }

    @Test fun `PokerStars file parser`() {
//        for(hand in path.open().read().hands())
//            println(parse(hand.lines()).join())
//        assertTrue(path.open().read().hands().all { parse(it.lines()).isRight() })
    }

    private val parse = Parse(Room.POKERSTARS)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokerstars.txt"
    private val input = """
        PokerStars Zoom Hand #234507805544:  Hold'em No Limit ($0.02/$0.05) - 2022/03/02 15:10:33 ET
        Table 'Donati' 6-max Seat #1 is the button
        Seat 1: kikis chair1 ($7.12 in chips)
        Seat 2: Jlydoman ($5.83 in chips)
        Seat 3: FIOMAD ($5.07 in chips)
        Seat 4: JulyanOsario ($2.48 in chips)
        Seat 5: energyvadym ($6.47 in chips)
        Seat 6: RusSuomi ($3.94 in chips)
        Jlydoman: posts small blind $0.02
        FIOMAD: posts big blind $0.05
        *** HOLE CARDS ***
        Dealt to energyvadym [Td Ts]
        JulyanOsario: calls $0.05
        energyvadym: raises $6.42 to $6.47 and is all-in
        RusSuomi: folds
        kikis chair1: folds
        Jlydoman: calls $5.81 and is all-in
        FIOMAD: folds
        JulyanOsario: folds
        Uncalled bet ($0.64) returned to energyvadym
        *** FIRST FLOP *** [7d Jd 7c]
        *** FIRST TURN *** [7d Jd 7c] [9d]
        *** FIRST RIVER *** [7d Jd 7c 9d] [2h]
        *** SECOND FLOP *** [5h 9c Jh]
        *** SECOND TURN *** [5h 9c Jh] [8c]
        *** SECOND RIVER *** [5h 9c Jh 8c] [Kc]
        *** FIRST SHOW DOWN ***
        Jlydoman: shows [Ah Ad] (two pair, Aces and Sevens)
        energyvadym: shows [Td Ts] (two pair, Tens and Sevens)
        Jlydoman collected $5.64 from pot
        *** SECOND SHOW DOWN ***
        Jlydoman: shows [Ah Ad] (a pair of Aces)
        energyvadym: shows [Td Ts] (a pair of Tens)
        Jlydoman collected $5.63 from pot
        *** SUMMARY ***
        Total pot $11.76 | Rake $0.49
        Hand was run twice
        FIRST Board [7d Jd 7c 9d 2h]
        SECOND Board [5h 9c Jh 8c Kc]
        Seat 1: kikis chair1 (button) folded before Flop (didn't bet)
        Seat 2: Jlydoman (small blind) showed [Ah Ad] and won ($5.64) with two pair, Aces and Sevens, and won ($5.63) with a pair of Aces
        Seat 3: FIOMAD (big blind) folded before Flop
        Seat 4: JulyanOsario folded before Flop
        Seat 5: energyvadym showed [Td Ts] and lost with two pair, Tens and Sevens, and lost with a pair of Tens
        Seat 6: RusSuomi folded before Flop (didn't bet)
    """.trimIndent()
}
