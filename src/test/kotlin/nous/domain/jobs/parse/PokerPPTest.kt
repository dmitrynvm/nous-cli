package nous.domain.jobs.parse

import nous.domain.core.*
import kotlin.test.*

class PokerPPTest {

    @Test fun `PPPoker hands parser`() {
//        assertTrue(parse(input.lines()).isRight())
    }

    private val parse = Parse(Room.POKERPP)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokerpp.txt"
    private val input = """
        PokerStars Hand #349041210000319: Hold'em No Limit ($0.50/$1 USD) - 2021/10/08 1:04:00 UTC
        Table 'PPP_34904121' 6-max Seat #4 is the button
        Seat 1: 3269911 ($67.72 in chips)
        Seat 2: 660471 ($108.68 in chips)
        Seat 3: 6549879 ($126.43 in chips)
        Seat 4: 6118161 ($73.20 in chips)
        Seat 6: 6528594 ($100 in chips)
        6528594: posts small blind $0.50
        3269911: posts big blind $1
        *** HOLE CARDS ***
        660471: folds
        6549879: folds 
        6118161: raises $1.50 to $2.50
        6528594: folds
        3269911: calls $1.50
        *** FLOP *** [7h Kd 7d] {Rake: $0}
        3269911: bets $1
        6118161: calls $1
        *** TURN *** [7h Kd 7d] [Ks] {Rake: $0}
        3269911: bets $1
        6118161: calls $1
        *** RIVER *** [7h Kd 7d Ks] [Qc] {Rake: $0}
        3269911: checks
        6118161: checks
        *** SHOW DOWN ***
        3269911: shows [9d 3d] (two pair, Kings and Sevens)
        6118161: shows [As 8s] (two pair, Kings and Sevens)
        6118161 collected $9.03 from pot
        *** SUMMARY ***
        Total pot $9.50 | Rake $0.47
        Board [7h Kd 7d Ks Qc]  
        Seat 1: 3269911 (big blind) showed [9d 3d] and lost with two pair, Kings and Sevens
        Seat 2: 660471 folded before Flop (didn't bet)
        Seat 3: 6549879 folded before Flop (didn't bet)
        Seat 4: 6118161 (button) showed [As 8s] and won ($9.03) with two pair, Kings and Sevens
        Seat 6: 6528594 (small blind) folded before Flop 
    """.trimIndent()
}
