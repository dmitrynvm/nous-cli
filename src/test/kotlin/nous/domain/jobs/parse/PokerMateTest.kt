package nous.domain.jobs.parse

import nous.domain.core.*
import kotlin.test.*

class PokerMateTest {

    @Test fun `PokerMate hand parsing`() {
//        assertTrue(parse(input.lines()).isRight())
    }

    @Test fun `PartyMate file parser`() {
//        for(hand in path.open().read().hands())
//            println(parse(hand.lines()).join())
//        assertTrue(path.open().read().hands().all { parse(it.lines()).isRight() })
    }

    private val parse = Parse(Room.POKERMATE)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokermate.txt"
    private val input = """
        581518  pokermate-fairplay #311588487657660416 (sb:200 bb:400 ante:200 speed:normal) time:2019-02-02 15:09:32
        ---seats---
        d088b88bdbdb4e10f0646b47a49fa8a3 seat:0  stack:86600
        570c23238c026384cb6e5afadb25b20d seat:1 co stack:53800
        7c501012fb40cf8bf8a568cc276a0764 seat:2 btn stack:31800
        408b0e00bb9c709a3c1ccee296084bc8 seat:3 sb stack:122900
        fecdece4c83378fc3c1da47414efc70b seat:4 bb stack:112000
        f9e56b44e88459a63b989d182cdfc716 seat:5 straddle stack:56800
        06cd06b0e2f34ffc6875d60db22a815c seat:6  stack:80000
        9be645f4d6334b975e9b6e8f3702f958 seat:7  stack:52200
        ------
        d088b88bdbdb4e10f0646b47a49fa8a3 ante:200
        570c23238c026384cb6e5afadb25b20d ante:200
        7c501012fb40cf8bf8a568cc276a0764 ante:200
        408b0e00bb9c709a3c1ccee296084bc8 ante:200
        408b0e00bb9c709a3c1ccee296084bc8 sb:200
        fecdece4c83378fc3c1da47414efc70b ante:200
        fecdece4c83378fc3c1da47414efc70b bb:400
        f9e56b44e88459a63b989d182cdfc716 ante:200
        f9e56b44e88459a63b989d182cdfc716 straddle:800
        06cd06b0e2f34ffc6875d60db22a815c ante:200
        9be645f4d6334b975e9b6e8f3702f958 ante:200
        06cd06b0e2f34ffc6875d60db22a815c post :800
        ---preflop---
        d088b88bdbdb4e10f0646b47a49fa8a3 holecards:sA cK
        570c23238c026384cb6e5afadb25b20d holecards:c7 d5 
        7c501012fb40cf8bf8a568cc276a0764 holecards:c5 c3
        408b0e00bb9c709a3c1ccee296084bc8 holecards:dJ dA
        fecdece4c83378fc3c1da47414efc70b holecards:c2 cQ
        f9e56b44e88459a63b989d182cdfc716 holecards:hQ s2
        06cd06b0e2f34ffc6875d60db22a815c holecards:sQ sK
        9be645f4d6334b975e9b6e8f3702f958 holecards:s9 d2
        collect pot stake:1600 [3,4,5,6,7,0,1,2,]
        06cd06b0e2f34ffc6875d60db22a815c bet 2500
        9be645f4d6334b975e9b6e8f3702f958 fold
        d088b88bdbdb4e10f0646b47a49fa8a3 call 3300
        570c23238c026384cb6e5afadb25b20d fold
        7c501012fb40cf8bf8a568cc276a0764 call 3300
        408b0e00bb9c709a3c1ccee296084bc8 call 3100
        fecdece4c83378fc3c1da47414efc70b fold
        f9e56b44e88459a63b989d182cdfc716 fold
        collect pot stake:16000 [4,5,6,7,0,1,2,3,]
        --flop cards:s7 h8 s3
        408b0e00bb9c709a3c1ccee296084bc8 check
        06cd06b0e2f34ffc6875d60db22a815c bet 8000
        d088b88bdbdb4e10f0646b47a49fa8a3 raise 18000
        7c501012fb40cf8bf8a568cc276a0764 fold
        408b0e00bb9c709a3c1ccee296084bc8 fold
        06cd06b0e2f34ffc6875d60db22a815c call 10000
        collect pot stake:52000 [0,1,2,3,4,5,6,7,]
        --turn cards:s7 h8 s3 h7
        06cd06b0e2f34ffc6875d60db22a815c check
        d088b88bdbdb4e10f0646b47a49fa8a3 bet 35000
        06cd06b0e2f34ffc6875d60db22a815c allin 58500
        d088b88bdbdb4e10f0646b47a49fa8a3 call 23500
        collect pot stake:169000 [0,1,2,3,4,5,6,7,]
        seat:0 can buy insurance, pot stake:169000 odds:2.2
        outs:s5,dQ,s4,sT,s6,sJ,s8,s9,cQ,hQ,s2
        --river cards:s7 h8 s3 h7 hT
        ---summary---
        9be645f4d6334b975e9b6e8f3702f958 win lose:-200
        d088b88bdbdb4e10f0646b47a49fa8a3 win lose:88600
        570c23238c026384cb6e5afadb25b20d win lose:-200
        7c501012fb40cf8bf8a568cc276a0764 win lose:-3500
        408b0e00bb9c709a3c1ccee296084bc8 win lose:-3500
        fecdece4c83378fc3c1da47414efc70b win lose:-600
        f9e56b44e88459a63b989d182cdfc716 win lose:-1000
        06cd06b0e2f34ffc6875d60db22a815c win lose:-80000
        
    """.trimIndent()
}
