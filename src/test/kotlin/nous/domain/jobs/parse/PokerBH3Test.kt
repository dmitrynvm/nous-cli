package nous.domain.jobs.parse

import nous.domain.core.*
import nous.domain.jobs.Rotate
import kotlin.test.*

class PokerBH3Test {
    @Test fun `PokerBH3 hands parser`() {
        val lines = input.lines()
//        assertTrue(parse(input.lines()).isRight())
    }

    @Test fun `PokerBH3 file parser`() {
//        for(hand in path.open().read().hands())
//            println(parse(hand.lines()).join())
//        assertTrue(path.open().read().hands().all { parse(it.lines()).isRight() })
    }

    private val parse = Parse(Room.POKERBH3)
    private val ratate = Rotate(Room.POKERBH3)
    private val path = "src/test/resources/nous/domain/jobs/parse/pokerbh3.txt"
    private val input = """
        #221373500: Texas Hold'em NL (4 р./8 р. RUB) - 2016/10/20 09:57:42 UTC
        Table 'Dubna Jackpot [ 40BB / 100BB ] 9Max #3324' Seat #3 is the button
        Seat 3: 54102bfd89051d144998268e (693.76 р. in chips)
        Seat 4: 5699170a6d9dc97bdd809152 (627.60 р. in chips)
        Seat 5: 57571aa2190eb9a425d032e7 (275.81 р. in chips)
        Seat 6: 55efd05a6d9dc97bdd37c78a (1,247.34 р. in chips)
        Seat 7: 53ebbfe389051d144990482a (814.47 р. in chips)
        Seat 8: 553d537d6d9dc97bddd778df (427.12 р. in chips)
        Seat 0: 57ffa05d50be63e9f4ccab03 (493.94 р. in chips)
        Seat 1: 55c1ad3f6d9dc97bddd1c259 (352 р. in chips)
        Seat 2: 56e89115190eb9a4253e288b (788 р. in chips)
        5699170a6d9dc97bdd809152: posts small blind 4 р.
        57571aa2190eb9a425d032e7: posts big blind 8 р.
        *** HOLE CARDS ***
        54102bfd89051d144998268e: [Ts 7s]
        5699170a6d9dc97bdd809152: [2h 4s]
        57571aa2190eb9a425d032e7: [4h 9h]
        55efd05a6d9dc97bdd37c78a: [6h Qc]
        53ebbfe389051d144990482a: [Kc 5s]
        553d537d6d9dc97bddd778df: [7h Qh]
        57ffa05d50be63e9f4ccab03: [6s 9s]
        55c1ad3f6d9dc97bddd1c259: [4d 5h]
        56e89115190eb9a4253e288b: [3s Ad]
        *** PREFLOP ***
        55efd05a6d9dc97bdd37c78a: folds
        53ebbfe389051d144990482a: folds
        553d537d6d9dc97bddd778df: calls 8 р.
        57ffa05d50be63e9f4ccab03: calls 8 р.
        55c1ad3f6d9dc97bddd1c259: folds
        56e89115190eb9a4253e288b: folds
        54102bfd89051d144998268e: calls 8 р.
        5699170a6d9dc97bdd809152: folds
        57571aa2190eb9a425d032e7: checks
        *** FLOP *** [9d 3d Kh]
        57571aa2190eb9a425d032e7: checks
        553d537d6d9dc97bddd778df: checks
        57ffa05d50be63e9f4ccab03: checks
        54102bfd89051d144998268e: checks
        *** TURN *** [9d 3d Kh] [Ks]
        57571aa2190eb9a425d032e7: bets 8 р.
        553d537d6d9dc97bddd778df: folds
        57ffa05d50be63e9f4ccab03: folds
        54102bfd89051d144998268e: folds
        57571aa2190eb9a425d032e7: uncalled bet 8 р.
        57571aa2190eb9a425d032e7 collected 33.89 р. from main pot
        *** SUMMARY ***
        Total pot 36 р.. | Rake 1.75 р.. Jackpot fee 0.36 р..
        Board [9d 3d Kh Ks]
    """.trimIndent()
}
