package nous.utils.math

import nous.domain.event.*
import nous.utils.io.*
import org.apache.commons.io.FileUtils
import java.io.File
import java.time.Duration
import kotlin.math.abs
import kotlin.math.max

const val oo: Double = Double.POSITIVE_INFINITY

fun age(duration: Duration): String = run {
    val seconds = duration.toSeconds()
    val minutes = duration.toMinutes()
    val hours = duration.toHours()
    val days = duration.toDays()

    if(days > 0)
        "$days days"
    else if(hours > 0)
        "$hours hours"
    else if(minutes > 0)
        "$minutes minutes"
    else
        "$seconds seconds"
}

fun File.length(units: String = "MB"): Double = when(units.uppercase()) {
    "GB" -> (this.length().toDouble() / (1024.0 * 1024.0 * 1024.0))
    "MB" -> (this.length().toDouble() / (1024 * 1024))
    "KB" -> (this.length().toDouble() / 1024)
    "B" -> (this.length().toDouble())
    else -> this.length().toDouble()
}

fun List<File>.length(units: String = "MB", decimals: Int = 2): Double =
    sumOf { it.length(units) }.round(decimals)

fun Double.round(n: Int = 2): Double {
    return "%.${n}f".format(this).toDouble()
}

fun Double.round2(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) { multiplier *= 10 }
    return kotlin.math.round(this * multiplier) / multiplier
}

fun measure(n: Long): String = FileUtils.byteCountToDisplaySize(n)

fun File.size(): String = FileUtils.byteCountToDisplaySize(FileUtils.sizeOfDirectory(this))

fun String.numberOfFiles(): Int = files().count()

fun String.numberOfHands(): Int = files().sumOf { file -> file.read().hands().size }

fun <T> List<T>.add(element: T): List<T> = this + listOf(element)

fun <T> List<T>.addAll(vararg elements: T): List<T> = this + elements

fun <T> List<T>.update(index: Int, item: T): List<T> =
    mapIndexed { i, el -> if(i == index) item else el }

fun List<Double>.max(): Double = maxOrNull() ?: oo

fun kron(cond: Boolean): Int = if(cond) 1 else 0

fun kron(a: Any, b: Any): Int = if(a == b) 1 else 0

fun List<Int>.asg(i: Int, x: Int): List<Int> =
    List(this.size) { if(it == i) x else this[it] }

fun List<Int>.inc(i: Int, x: Int): List<Int> =
    List(this.size) { if(it == i) this[it] + x else this[it] }

infix fun List<Int>.add(other: List<Int>): List<Int> =
    zip(other).map { (a, b) -> a + b }

infix fun List<Int>.sub(other: List<Int>): List<Int> =
    zip(other).map { (a, b) -> a - b }

infix fun List<Int>.eq(other: List<Int>): List<Int> =
    zip(other).map { (a, b) -> kron(a, b) }

infix fun List<Int>.or(other: List<Int>): List<Int> =
    zip(other).map { (a, b) -> max(a, b) }

infix fun List<Int>.and(other: List<Int>): List<Int> =
    zip(other).map { (a, b) -> a * b }

fun Int.toBoolean(): Boolean = this == 1

fun <T> List<List<T>>.transpose(): List<List<T>> {
    val result = (first().indices).map { mutableListOf<T>() }.toMutableList()
    forEach { list -> result.zip(list).forEach { it.first.add(it.second) } }
    return result
}

object Cmp : Comparator<Result<HoldemEvent>> {
    override fun compare(a: Result<HoldemEvent>, b: Result<HoldemEvent>): Int = run {
        val aa = a.getOrElse { NoneEvent() }
        val bb = b.getOrElse { NoneEvent() }
        when {
            aa is PlayerEvent && bb is PlayerEvent -> aa.seat - bb.seat
            else -> aa.ordinal - bb.ordinal
        }
    }
}

fun List<Int>.closest(input: Int): Int = minByOrNull { abs(input - it) } ?: -1


fun genCombs(m: Int, n: Int, i: Int, comb: IntArray, combs: MutableList<IntArray>) {
    if (i >= m) {
        combs.add(comb.clone())
    }
    else {
        for (j in 0 until n) {
            if (i == 0 || j > comb[i - 1]) {
                comb[i] = j
                genCombs(m, n,i+1, comb, combs)
            }
        }
    }
}

fun rndCombs(k: Int, m: Int, n: Int): ArrayList<IntArray> {
    val combs = ArrayList<IntArray>()
    for (i in 0 until k) {
        val comb = ArrayList<Int>(m)
        while(comb.size < m) {
            val number = (0 until n).random()
            if(!comb.contains(number))
                comb.add(number)
        }
        combs.add(comb.toIntArray())
    }
    return combs
}

fun allCombs(m: Int, n: Int): ArrayList<IntArray> {
    val comb = IntArray(m)
    val combs = ArrayList<IntArray>()
    genCombs(m, n, 0, comb, combs)
    return combs
}