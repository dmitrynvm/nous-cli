package nous.utils.fmt

import com.hubspot.jinjava.Jinjava
import com.google.gson.GsonBuilder
import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.yaml.YamlPropertySource
import de.vandermeer.asciitable.AsciiTable
import de.vandermeer.asciithemes.TA_GridThemes
import io.github.serpro69.kfaker.*
import nous.application.config.ParsePatternConfig
import nous.domain.core.*
import nous.utils.io.lines
import nous.utils.io.open
import nous.utils.io.pwd
import nous.utils.io.read
import java.time.LocalDateTime
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger
import java.util.regex.Matcher
import kotlin.reflect.full.memberProperties

val jinjava = Jinjava()
val cache = ConcurrentHashMap<String, String>()
val faker = Faker()
val config = fakerConfig {
    uniqueGeneratorRetryLimit = 1000
}
var gameCount = AtomicInteger()

fun String.render(map: Map<String, String>): String = jinjava.render(this, map)

fun Matcher.getOrElse(input: String, default: String = ""): String =
    try {
        group(input).ifBlank { default }
    } catch (_: Exception) {
        default
    }

fun Boolean.toInt(): Int = if (this) 1 else 0

fun String.toInteger(): Int = if (isBlank()) 0 else toInt()

fun String.toBonus(): Bonus = Bonus(this)

fun String.toBranch(): Branch = Branch(this)

fun String.toCards(): Cards = Cards(this)

fun String.toCombo(): Combo = Combo(this)

fun String.toCurrency(): Currency = Currency(this)

fun String.toDate(): Date = Date(this)

fun String.toMode(): Mode = Mode(this)

fun String.toAmount(): Amount = Amount(this)

fun String.toPosition(): Position = Position(this)

fun String.toRoom(): Room = Room(this)

fun String.toSpeed(): Speed = Speed(this)

fun String.toType(): Kind = Kind(this)

fun String.toZone(): Zone = Zone(this)

fun now(): String = LocalDateTime.now().toString()

fun random(): Char = (('A'..'Z') + ('a'..'z') + ('0'..'9')).random()

fun guid(): String = gameCount.incrementAndGet().toString().padStart(9, '0')

fun duid(): String = faker.heroes.names().replace(" ", "").lowercase()

fun uuid(name: String): String = run {
    if (cache.containsKey(name))
        cache[name].toString()
    else {
        val value = faker.name.nameWithMiddle().lowercase().remove(" ", ".", ",").slice(0..7)
        cache.putIfAbsent(name, value)
        value
    }
}

fun String.lowers(): String = filter { it.isLowerCase() }

fun String.uppers(): String = filter { it.isUpperCase() or it.isDigit() }

fun String.replacep(pairs: List<Pair<String, String>>): String =
    pairs.fold(this) { acc, (old, new) -> acc.replace(old, new) }

fun String.replace(map: Map<String, String>): String =
    map.entries.fold(this) { acc, (key, value) -> acc.replace(key, value) }

fun String.replace(codes: List<Int>): String =
    replacep(codes.map { Char(it).toString() to "" }.toList())

fun String.remove(vararg inputs: String): String =
    replace(inputs.associateWith { "" })

fun <T> Any.toNode(room: Room = Room.NONE): Map<String, T> = run {
    this::class.memberProperties
        .filter {
            val elem = it.getter.call(this).toString()
            elem.isNotBlank() && !elem.contains("NONE")
        }
        .associate {
            val element = it.getter.call(this)
            if (element is Entity)
                it.name to element.toString(room) as T
            else
                it.name to element.toString() as T
        }.filterKeys { it != "ordinal" }
}

inline fun <reified T : Any> T.toJson(prefix: String = ""): String = run {
//    val prefix = name.ifEmpty { T::class.java.name.split(".").last() }
    val builder = GsonBuilder().serializeSpecialFloatingPointValues()
    builder.setPrettyPrinting()
    prefix + builder.create().toJson(this)
}

fun Array<Array<String>>.toTable(): String {
    val output = AsciiTable()
    val width = this[0].size * 20
    output.context.setGridTheme(TA_GridThemes.FULL)
    output.context.width = width
    output.addRule()
    for ((i, row) in this.withIndex()) {
        output.addRow(*row)
        if (i == 0)
            output.addRule()
    }
    output.addRule()
    return output.render()
}

fun join(vararg elements: String): String = elements.joinToString("/")

fun List<*>.join(separator: String = "\n", prefix: String = "", postfix: String = "\n"): String =
    joinToString(separator = separator, prefix = prefix, postfix = postfix)

fun buildTemplate(room: Room): Map<String, String> = run {
    val input = pwd("jobs/parse/${room.name.lowercase()}.yml").open().read()
    val n = input.lines().indexOf("pattern:") + 1
    val exclude = listOf("say", "said", "allow", "connect", "disconn", "join", "leave", "sitout", "fail")
    val lines = input.lines().filter { line -> exclude.all { key -> !line.startsWith(key) } }
    val inp = lines.slice(n until lines.size).join("\n")

    val p = "\\$\\{(?<v>.*?)\\}"
    val m = p.toPattern().matcher(inp)
    val bgn = mutableListOf<Pair<String, String>>()
    val end = mutableListOf(
        "^" to "",
        "$" to "",
        "\\" to ""
    )

    while(m.find()) {
        val v = m.group("v")
        val w = v.split(".").last()
        val old = "\${$v}"
        val new = "{{$w}}"
        bgn.add(old to new)
    }
    ConfigLoaderBuilder.default()
        .addSource(YamlPropertySource(inp.replacep(bgn).replacep(end)))
        .build()
        .loadConfigOrThrow<ParsePatternConfig>()
        .toMap()
}
