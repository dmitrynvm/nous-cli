package nous.utils.hash

import org.apache.commons.codec.digest.DigestUtils
import java.util.*

val hashids = Hashids(random(10), 7)

fun String.toSha256(): String = DigestUtils.sha256Hex(this)

fun String.toBase64(): String = Base64.getMimeEncoder().encodeToString(toByteArray())

fun String.fromBase64(): String = String(Base64.getMimeDecoder().decode(this))

fun random(length: Int): String {
    val charset = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz0123456789"
    return (1..length)
        .map { charset.random() }
        .joinToString("")
}

fun hashid(input: String): String = hashids.encode(input.sumOf { it.code.toLong() })
