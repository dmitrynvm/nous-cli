package nous.utils.io

import nous.utils.fmt.*
import com.sksamuel.hoplite.ConfigLoader
import org.apache.commons.io.FileUtils
import me.tongfei.progressbar.*
import kotlin.io.path.*
import java.io.File
import java.nio.file.*

fun buildProgress(name: String, n: Int): ProgressBar = ProgressBarBuilder()
    .setTaskName("Convert")
    .setUpdateIntervalMillis(1)
    .setInitialMax(n.toLong())
    .setStyle(ProgressBarStyle.ASCII)
    .build()

fun File.dirs(depth: Int = 1) : List<File> =
    walk()
    .maxDepth(depth)
    .filter { it.name != this.name && it.isDirectory && !it.isHidden }
    .toList()

fun Path.dirs(depth: Int = 1) : List<File> = File(toString()).dirs(depth)

fun String.dirs(depth: Int = 1) : List<File> = File(this).dirs(depth)

fun File.files(ext: String = "txt") : List<File> =
    walk()
    .filter { it.isFile && it.extension == ext }
    .toList()

fun Path.files(ext: String = "txt") : List<File> = File(toString()).files(ext)

fun String.files(ext: String = "txt") : List<File> = File(this).files(ext)

fun File.read() : String = readText()

fun Path.open() : File = File(this.toString())
fun String.open() : File = File(this)

fun Path.read() : String = File(toString()).readText()

fun String.read() : String = File(this).readText()

fun String.exists(): Boolean =
    File(this).let { it.exists() && !it.isDirectory }

fun File.write(input: String, end: String = "\n") = appendText("$input$end")

fun dev(vararg subpaths: String): Path = Path("src/main/resources", *subpaths)

fun sys(vararg subpaths: String): Path = Path(System.getenv("NOUS_HOME"), *subpaths)

fun cur(vararg subpaths: String): Path = Path("", *subpaths).toAbsolutePath()

fun pwd(filename: String = ""): Path =
    if(dev("config").exists())
        dev("config", filename)
    else if(cur(".nous", filename).exists())
        cur(".nous", filename)
    else sys("config", filename)

fun copy(source: Path, target: Path) =
    FileUtils.copyDirectory(File(source.toString()), File(target.toString()))

fun rm(target: Path) = FileUtils.deleteDirectory(File(target.toString()))

fun rm(target: File) = FileUtils.deleteDirectory(target)

fun mkdir(file: File, overwrite: Boolean = true) = run {
    if (overwrite) {
        FileUtils.deleteDirectory(file)
        FileUtils.forceMkdir(file)
    } else {
        FileUtils.forceMkdir(file)
    }
}

fun mkdir(path: Path, overwrite: Boolean = true) = run {
    if(overwrite) {
        FileUtils.deleteDirectory(File(path.toString()))
        FileUtils.forceMkdir(File(path.toString()))
    }
    else {
        FileUtils.forceMkdir(File(path.toString()))
    }
}

fun mkdir(path: String, overwrite: Boolean = true) = run {
    if(overwrite) {
        FileUtils.deleteDirectory(File(path))
        FileUtils.forceMkdir(File(path))
    }
    else {
        FileUtils.forceMkdir(File(path))
    }
}

inline fun <reified T: Any> fromYaml(path: Path) =
    ConfigLoader().loadConfigOrThrow<T>(path.toString())

fun String.lines(
    separator: String = "\n",
    codes: List<Int> = listOf(160, 65279)
) = splitToSequence(separator)
        .filter { it.isNotBlank() }
        .map { it.replace(codes).trim() }
        .toList()

fun String.hands(sep: String = "(?:\\r?\\s?\\n){2,}"): List<String> =
        splitToSequence(sep.toRegex())
        .map { it.trim() + '\n' }
        .filter { it.isNotBlank() }
        .toList()
