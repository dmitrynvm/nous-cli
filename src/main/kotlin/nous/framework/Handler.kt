package nous.framework

typealias Handler<State, Event> = (state: State, event: Event) -> State
