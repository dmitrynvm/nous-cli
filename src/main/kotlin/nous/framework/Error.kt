package nous.framework

interface Error {
    override fun toString(): String
}
