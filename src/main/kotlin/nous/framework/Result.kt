package nous.framework

typealias Result<A> = Either<Error, A>
