package nous.framework

interface Job<in A, out B> {
    operator fun invoke(input: A): B
}