package nous.framework

import nous.domain.core.*

interface Event {
    fun toMap(room: Room = Room.NONE): Map<String, String>
}

