package nous.framework

import nous.utils.fmt.*

sealed class Either<out A, out B> {
    internal abstract val isRight: Boolean
    internal abstract val isLeft: Boolean

    fun isLeft(): Boolean = isLeft

    fun isRight(): Boolean = isRight

    inline fun <C> fold(ifLeft: (A) -> C, ifRight: (B) -> C): C = when (this) {
        is Right -> ifRight(value)
        is Left -> ifLeft(value)
    }

    inline fun <C> mapLeft(f: (A) -> C): Either<C, B> =
        fold({ Left(f(it)) }, { Right(it) })

    inline fun <C> mapRight(f: (B) -> C): Either<A, C> =
        fold({ Left(it) }, { Right(f(it)) })

    inline fun <C, D> mapBoth(leftOperation: (A) -> C, rightOperation: (B) -> D): Either<C, D> =
        fold({ Left(leftOperation(it)) }, { Right(rightOperation(it)) })

    inline fun exists(predicate: (B) -> Boolean): Boolean =
        fold({ false }, predicate)

    fun orNull(): B? = fold({ null }, { it })

    inline fun all(predicate: (B) -> Boolean): Boolean =
        fold({ true }, predicate)

    fun isEmpty(): Boolean = isLeft

    fun isNotEmpty(): Boolean = isRight

    data class Left<out A> constructor(val value: A) : Either<A, Nothing>() {
        override val isLeft = true
        override val isRight = false

        override fun toString(): String = if(value is List<*>) value.join() else "-$value"
    }

    data class Right<out B> constructor(val value: B) : Either<Nothing, B>() {
        override val isLeft = false
        override val isRight = true

        override fun toString(): String = if(value is List<*>) value.join() else "+$value"
    }
}

fun <A> identity(a: A): A = a

inline fun <B> Either<*, B>.getOrElse(default: () -> B ): B =
    fold({ default() }, ::identity)

fun <A, B> Either<A, B>.contains(elem: B): Boolean =
    exists { it == elem }

fun <A> A.toLeft(): Either<A, Nothing> = Either.Left(this)

fun <A> A.toRight(): Either<Nothing, A> = Either.Right(this)

fun <A, B> List<Either<A, B>>.isLeft(): Boolean = this.any { it.isLeft() }

fun <A, B> List<Either<A, B>>.isRight(): Boolean = this.all { it.isRight() }

fun <A, B, C> List<Either<A, B>>.mapLeft(f: (A) -> C): List<Either<C, B>> = map { it.mapLeft(f) }

fun <A, B, D> List<Either<A, B>>.mapRight(g: (B) -> D): List<Either<A, D>> = map { it.mapRight(g) }

fun <A, B, C, D> List<Either<A, B>>.mapBoth(f: (A) -> C, g: (B) -> D): List<Either<C, D>> = map { it.mapBoth(f, g) }

fun <B> List<Either<*, B>>.getOrElse(default: B): List<B> = map { it.getOrElse { default } }

//fun <A> A.isScalar(): Boolean = when(this) {
//    is Int -> true
//    is Float -> true
//    is Double -> true
//    is String -> this.lines().size <= 1
//    else -> false
//}
