package nous.domain.event

import nous.domain.core.*
import nous.framework.*
import nous.utils.fmt.toNode

//---------------------------------< BASE >-------------------------------//
sealed class HoldemEvent : Event {
    open val type: String = ""
    open val ordinal: Int = 0
    override fun toString(): String = ""
}

sealed class SecretEvent : HoldemEvent() {
    open val name: String = ""
    open val seat: Int = 0
    open val position: Position = Position.NONE
    abstract fun setName(name: String): SecretEvent
    abstract fun setSeat(seat: Int): SecretEvent
    abstract fun setData(name: String, seat: Int, position: Position): SecretEvent
}

sealed class StreetEvent : HoldemEvent() {
    open val cards: Cards = Cards()
}

sealed class StakeEvent : SecretEvent() {
    override val name: String = ""
    open val amount: Amount = Amount()
}

//---------------------------------< INIT >-------------------------------//
data class TitleEvent(
    val room: Room = Room.NONE,
    val name: String = "",
    val kind: Kind = Kind.NOLIMIT_HOLDEM,
    val speed: Speed = Speed.NORMAL,
    val sb: Amount = Amount(),
    val bb: Amount = Amount(),
    val currency: Currency = Currency.USD,
    val date: Date = Date(),
    val bonus: Bonus = Bonus.RAKE,
    val zone: Zone = Zone.UTC,
    override val type: String = "title",
    override val ordinal: Int = 0
) : HoldemEvent() {
    override fun toString(): String =
        "Title(room=$room, name=$name, kind=${kind}, speed=$speed, sb=$sb, bb=$bb, currency=$currency, date=$date, bonus=$bonus, zone=$zone)"
    fun toMap(): Map<String, String> = toNode()
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    fun setName(name: String) = copy(name = name)
}

data class HeaderEvent(
    val room: Room,
    val name: String,
    val speed: Speed,
    val bonus: Bonus,
    override val type: String = "header",
    override val ordinal: Int = 1
) : HoldemEvent() {
    override fun toString(): String =
        "Header(room=$room, name=$name, speed=$speed, bonus=$bonus)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    fun setName(name: String) = copy(name = name)
}

data class FooterEvent(
    val sb: Amount,
    val bb: Amount,
    val kind: Kind,
    val date: Date,
    val zone: Zone = Zone.UTC,
    override val type: String = "footer",
    override val ordinal: Int = 2
) : HoldemEvent() {
    override fun toString(): String =
        "Footer(sb=$sb, bb=$bb, kind=$kind, date=$date, zone=$zone)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class DealerEvent(
    val name: String,
    val seat: Int = 1,
    val position: Position = Position.BTN,
    val size: Int,
    val mode: Mode = Mode.CASH,
    override val type: String = "dealer",
    override val ordinal: Int = 3
) : HoldemEvent() {
    override fun toString(): String =
        "Dealer(name=$name, seat=$seat, position=$position, size=$size, mode=$mode)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    fun setName(name: String) = copy(name = name)
    fun setSeat(seat: Int) = copy(seat = seat)
    fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class ButtonEvent(
    val seat: Int,
    override val type: String = "button",
    override val ordinal: Int = 4
) : HoldemEvent() {
    override fun toString(): String =
        "Button(seat=$seat)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    fun setSeat(seat: Int) = copy(seat = seat)
}

data class TableEvent(
    val size: Int,
    override val type: String = "table",
    override val ordinal: Int = 5
) : HoldemEvent() {
    override fun toString(): String =
        "Table(size=$size)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class PlayerEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position,
    val stack: Amount,
    override val type: String = "player",
    override val ordinal: Int = 6
) : SecretEvent(){
    override fun toString(): String =
        "Player(name=$name, seat=$seat, position=$position, stack=$stack)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class HeroEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val cards: Cards,
    override val type: String = "hero",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Hero(name=$name, seat=$seat, position=$position, cards=$cards)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

//---------------------------------< MOVE >-------------------------------//
data class SeatingEvent(
    override val cards: Cards = Cards(),
    override val type: String = "seating",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String = "Seating()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class PostingEvent(
    override val cards: Cards = Cards(),
    override val type: String = "posting",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String = "Posting()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class DealingEvent(
    override val cards: Cards = Cards(),
    override val type: String = "dealing",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String = "Dealing()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class PreflopEvent(
    override val cards: Cards = Cards(),
    override val type: String = "preflop",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String = "Preflop()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class FlopEvent(
    override val cards: Cards = Cards(),
    val branch: Branch,
    override val type: String = "flop",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String =
        "Flop(cards=$cards${if(branch.isNotEmpty()) ", branch=$branch" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class TurnEvent(
    override val cards: Cards = Cards(),
    val more: Cards = Cards(),
    val branch: Branch = Branch.NONE,
    override val type: String = "turn",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String =
        "Turn(cards=$cards${if(branch.isNotEmpty()) ", branch=$branch" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class RiverEvent(
    override val cards: Cards = Cards(),
    val more: Cards = Cards(),
    val branch: Branch = Branch.NONE,
    override val type: String = "river",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString() =
        "River(cards=$cards${if(branch.isNotEmpty()) ", branch=$branch" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class ShowdownEvent(
    override val cards: Cards = Cards(),
    val branch: Branch = Branch.NONE,
    override val type: String = "showdown",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString() =
        "Showdown(${if(branch.isNotEmpty()) "branch=$branch" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class SummaryEvent(
    override val cards: Cards = Cards(),
    override val type: String = "summary",
    override val ordinal: Int = 7
) : StreetEvent() {
    override fun toString(): String = "Summary()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

data class ForkEvent(
    override val type: String = "fork",
    override val ordinal: Int = 7
) : HoldemEvent() {
    override fun toString(): String =
        "Fork()"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

//---------------------------------< STAKE >-------------------------------//
data class AnteEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "ante",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Ante(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class SmallBlindEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "small_blind",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "SmallBlind(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class BigBlindEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "big_blind",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "BigBlind(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class StraddleEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "straddle",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Straddle(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class StriddleEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "striddle",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Striddle(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class DeadBlindEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "dead_blind",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "DeadBlind(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class FoldEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount = Amount(0.0),
    override val type: String = "fold",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString() = "Fold(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class FoldShowEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val cards: Cards = Cards(),
    override val amount: Amount = Amount(0.0),
    override val type: String = "fold",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString() = "Fold(name=$name, seat=$seat, position=$position, cards=$cards)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class CheckEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount = Amount(),
    override val type: String = "call",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String = "Check(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class BetEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "bet",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Bet(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class CallEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "call",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Call(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class RaiseEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    val other: Amount = Amount(0.0),
    override val type: String = "raise",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString() =
        "Raise(name=$name, seat=$seat, position=$position${if(other > 0) ", other=$other" else ""}, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class PushEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val amount: Amount,
    override val type: String = "push",
    override val ordinal: Int = 7
) : StakeEvent() {
    override fun toString(): String =
        "Push(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class InsureEvent(
    override val type: String = "insure",
    val seat: Int,
    override val ordinal: Int = 7
) : HoldemEvent() {
    override fun toString(): String =
        "Insure(seat=$seat)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}

//-------------------------------< RESULTS >-----------------------------//
data class HideEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "hide",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Hide(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class MuckEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val cards: Cards,
    override val type: String = "muck",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Muck(name=$name, seat=$seat, position=$position, cards=$cards)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class ShowEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val cards: Cards,
    val combo: Combo,
    override val type: String = "show",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Show(name=$name, seat=$seat, position=$position, cards=$cards, combo=$combo)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class UncallEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val amount: Amount,
    override val type: String = "uncall",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Uncall(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class TakeEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val amount: Amount,
    val side: Int = 0,
    val players: String = "",
    override val type: String = "take",
    override val ordinal: Int = 8
) : SecretEvent() {
    override fun toString() =
        "Take(name=$name, seat=$seat, position=$position, amount=$amount${if(side > 0)", side=$side" else ""}${if(players.isNotBlank())", players=$players" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class CashoutEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val amount: Amount,
    val fee: Amount,
    override val type: String = "cashout",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Cashout(name=$name, seat=$seat, position=$position, amount=$amount, fee=$fee)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class TotalEvent(
    val amount: Amount,
    val rake: Amount = Amount(0.0),
    val jack: Amount = Amount(0.0),
    val bingo: Amount = Amount(0.0),
    override val type: String = "total",
    override val ordinal: Int = 7
) : HoldemEvent() {
    override fun toString() =
        "Total(amount=$amount${if(rake > 0)", rake=$rake" else ""}${if(jack > 0)", jack=$jack" else ""}${if(bingo > 0)", bingo=$bingo" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}
data class BoardEvent(
    val cards: Cards,
    val branch: Branch = Branch.NONE,
    override val type: String = "board",
    override val ordinal: Int = 7
) : HoldemEvent() {
    override fun toString() =
        "Board(cards=$cards${if(branch.isNotEmpty())", branch=$branch" else ""})"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
}
data class ShowedEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    val cards: Cards,
    override val type: String = "showed",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Showed(name=$name, seat=$seat, position=$position, cards=$cards)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class FoldedEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position,
    override val type: String = "folded",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString() =
        "Folded(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class WinEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    val cards: Cards,
    val pot: Amount,
    val amount: Amount,
    override val type: String = "win",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString() =
        "Winner(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class LoseEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    val cards: Cards,
    val amount: Amount,
    override val type: String = "lose",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString() =
        "Loser(name=$name, seat=$seat, position=$position, amount=$amount)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class TakenEvent(
    override val name: String = "",
    override val seat: Int,
    override val position: Position = Position.NONE,
    override val type: String = "taken",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Taken(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class MuckedEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    override val type: String = "mucked",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Mucked(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

//-------------------------------< NETWORK >-----------------------------//
data class SayEvent(
    override val name: String,
    override val seat: Int = 0,
    override val type: String = "say",
    override val position: Position = Position.NONE,
    val msg: String,
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Say(name=$name, seat=$seat, position=$position, msg=$msg)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class SaidEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    val msg: String,
    override val type: String = "said",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Said(name=$name, seat=$seat, position=$position, msg=$msg)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class JoinEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    override val type: String = "join",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Join(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class LeaveEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "leave",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Leave(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class AllowEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "allow",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Allow(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class FailEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "fail",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Fail(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class ConnectEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "connect",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Connect(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class DisconnEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "disconn",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Disconn(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class SitinEvent(
    override val name: String,
    override val seat: Int = 0,
    override val position: Position = Position.NONE,
    override val type: String = "sitin",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Sitin(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}
data class SitoutEvent(
    override val name: String,
    override val seat: Int,
    override val position: Position = Position.NONE,
    override val type: String = "sitout",
    override val ordinal: Int = 7
) : SecretEvent() {
    override fun toString(): String =
        "Sitout(name=$name, seat=$seat, position=$position)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override fun setName(name: String) = copy(name = name)
    override fun setSeat(seat: Int) = copy(seat = seat)
    override fun setData(name: String, seat: Int, position: Position) = copy(name = name, seat = seat, position = position)
}

data class NoneEvent(
    override val type: String = "none",
    val text: String = ""
) : HoldemEvent() {
    override fun toString(): String =
        "None($text)"
    override fun toMap(room: Room): Map<String, String> = toNode(room)
    override val ordinal: Int = 8
}
