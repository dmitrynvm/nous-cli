package nous.domain.config

import nous.utils.fmt.toNode

data class ParseConfig(
    val name: String  = "",
    val author: String = "",
    val description: String = "",

    val env: ParseEnvConfig = ParseEnvConfig(),
    val separator: ParseSeparatorConfig = ParseSeparatorConfig(),
    val pattern: ParsePatternConfig = ParsePatternConfig()
) {
    fun toMap(): Map<String, Any> = toNode()
}