package nous.domain.config

import nous.utils.fmt.toNode

data class ParsePatternConfig(

    // init
    val title: String = "",
    val header: String = "",
    val footer: String = "",
    val button: String = "",
    val table: String = "",
    val hero: String = "",
    val dealer: String = "",
    val player: String = "",

    // move
    val seating: String = "",
    val posting: String = "",
    val dealing: String = "",
    val preflop: String = "",
    val flop: String = "",
    val turn: String = "",
    val river: String = "",
    val showdown: String = "",
    val summary: String = "",

    // stake
    val ante: String = "",
    val smallBlind: String = "",
    val bigBlind: String = "",
    val straddle: String = "",
    val striddle: String = "",
    val deadBlind: String = "",
    val twinBlind: String = "",
    val fold: String = "",
    val foldShow: String = "",
    val check: String = "",
    val bet: String = "",
    val call: String = "",
    val raise: String = "",
    val push: String = "",
    val insure: String = "",

    // showdown
    val hide: String = "",
    val muck: String = "",
    val show: String = "",
    val uncall: String = "",
    val board: String = "",
    val total: String = "",
    val take: String = "",
    val cashout: String = "",
    val fork: String = "",

    // summary
    val folded: String = "",
    val hidden: String = "",
    val showed: String = "",
    val win: String = "",
    val lose: String = "",
    val taken: String = "",

    // network
    val say: String = "",
    val said: String = "",
    val connect: String = "",
    val disconn: String = "",
    val join: String = "",
    val leave: String = "",
    val allow: String = "",
    val fail: String = "",
    val sitout: String = "",

    val none: String = ""
) {
    fun toMap(): Map<String, String> = toNode()
}