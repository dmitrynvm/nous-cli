package nous.domain.config

import nous.utils.fmt.toNode

data class ParseEnvConfig(
    val at: String = "",
    val allin: String = "",
    val amount: String = "",
    val namount: String = "",
    val base: String = "",
    val b: String = "",
    val bb: String = "",
    val bingo: String = "",
    val bonus: String = "",
    val branch: String = "",
    val cards: String = "",
    val combo: String = "",
    val currency: String = "",
    val date: String = "",
    val fee: String = "",
    val jack: String = "",
    val kind: String  = "",
    val msg: String = "",
    val name: String = "",
    val number: String = "",
    val mode: String = "",
    val more: String = "",
    val other: String = "",
    val pot: String = "",
    val position: String = "",
    val rake: String = "",
    val room: String = "",
    val sb: String = "",
    val side: String = "",
    val size: String = "",
    val stack: String = "",
    val seat: String = "",
    val speed: String = "",
    val zone: String = "",
    val uuid: String = "",
) {
    fun toMap(): Map<String, String> = toNode()
}