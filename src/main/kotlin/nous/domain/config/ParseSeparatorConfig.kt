package nous.domain.config

import nous.utils.fmt.toNode

data class ParseSeparatorConfig(
    val line: String = "",
    val hand: String = ""
) {
    fun toMap(): Map<String, String> = toNode()
}