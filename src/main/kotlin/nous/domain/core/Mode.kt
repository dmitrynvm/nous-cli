package nous.domain.core

enum class Mode : Entity {
    TRAIN,
    TOUR,
    SNG,
    CASH,
    NONE;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String =
        when(room) {
            Room.POKERSTARS -> when(this) {
                CASH -> ""
                else -> "else"
            }
            Room.POKER888 -> when(this) {
                CASH -> "Real Money"
                else -> "else"
            }
            else -> name.lowercase()
        }

    companion object {
        operator fun invoke(s: String): Mode = when (s.replace(" ", "_").uppercase()) {
            "PRACTICE_PLAY" -> TRAIN
            "TOURNAMENT"    -> TOUR
            else            -> CASH
        }
    }
}
