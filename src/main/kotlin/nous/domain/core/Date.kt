package nous.domain.core

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class Date(private val element: LocalDateTime = LocalDateTime.MIN) : Entity {

    override fun toString(): String = element.toString()

    override fun toString(room: Room): String =
        when(room) {
            Room.POKERSTARS -> element.format(DateTimeFormatter.ofPattern("yyyy/M/dd H:mm:ss"))
            Room.POKER888 -> element.format(DateTimeFormatter.ofPattern("dd MM yyyy HH:mm:ss"))
            else -> element.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))
        }

    companion object {
        private val formats: List<Pair<String, String>> = listOf(
        "(\\d{2} \\d{2} \\d{4} \\d{2}:\\d{2}:\\d{2})" to "dd M yyyy H:mm:ss",
        "\\d{4}/\\d{1,2}/\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}" to "yyyy/M/dd H:mm:ss",
        "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}" to "yyyy-MM-dd HH:mm:ss"
        )
        operator fun invoke(input: String)  =
            Date(
                formats.first { it.first.toPattern().matcher(input).find() }
                .let { LocalDateTime.parse( input, DateTimeFormatter.ofPattern(it.second)) }
            )
    }
}
