package nous.domain.core

enum class Street {
    INITING,
    SEATING,
    POSTING,
    DEALING,
    PREFLOP,
    FLOP,
    TURN,
    RIVER,
    SHOWDOWN,
    SUMMARY,
    NONE;
}
