package nous.domain.core

object Deck {
    const val NUM_CARDS = 52
    const val NUM_RANKS = 13
    const val NUM_SUITS = 4

    val primes = intArrayOf(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41)
    val cards: Array<Card> = Card.all().toSet().toTypedArray()
    private var count = 0

    init {
        shuffle()
    }

    fun pick(): Card = cards[count++]

    fun pick(size: Int): Cards = Cards(Array(size) { cards[count++] })

    fun shuffle(): Deck {
        cards.shuffle()
        count = 0
        return this
    }

    override fun toString(): String = cards.contentToString()
}
