package nous.domain.core

enum class Zone : Entity {
    ET,
    CET,
    UTC,
    GMT,
    NONE;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String = name.lowercase()

    companion object {
        operator fun invoke(input: String): Zone = when (input.uppercase()) {
            "ET" -> ET
            "CET" -> CET
            "GMT" -> GMT
            "UTC" -> UTC
            else -> ET
        }
    }
}
