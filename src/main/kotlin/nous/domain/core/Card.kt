package nous.domain.core

class Card (private val element: Int) {

    constructor(rank: Rank, suit: Suit): this(rank.toInt() + Deck.NUM_RANKS * suit.toInt())

    constructor(s: String): this(Rank(s.substring(0, 1)), Suit(s.substring(1, 2)))

    val rank: Rank get() = Rank(element % Deck.NUM_RANKS)

    val suit: Suit get() = Suit(element / Deck.NUM_RANKS)

    override fun equals(other: Any?): Boolean = other is Card && other.element == this.element

    override fun hashCode(): Int = element

    fun toInt(): Int = element

    override fun toString(): String = rank.toString() + suit.toString()

    companion object {
        fun all(n: Int = Deck.NUM_CARDS): Sequence<Card> = sequence {
            for (i in 0 until n)
                yield(Card(i))
        }

        fun any(n: Int): Sequence<Card> = sequence {
            for (i in 0 until n)
                yield(Card((0 until Deck.NUM_CARDS).random()))
        }
    }
}
