package nous.domain.core

enum class Position : Entity {
    BTN,
    SB,
    BB,
    UTG,
    UTG1,
    MP,
    MP1,
    LJ,
    HJ,
    CO,
    NONE;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String = name.lowercase()

    fun isNotEmpty(): Boolean = this != NONE

    companion object {
        val table = arrayOf(
            arrayOf(BTN),
            arrayOf(BTN, SB),
            arrayOf(BTN, SB, BB),
            arrayOf(BTN, SB, BB, CO),
            arrayOf(BTN, SB, BB, MP, CO),
            arrayOf(BTN, SB, BB, UTG, MP, CO),
            arrayOf(BTN, SB, BB, UTG, MP, HJ, CO),
            arrayOf(BTN, SB, BB, UTG, MP, LJ, HJ, CO),
            arrayOf(BTN, SB, BB, UTG, UTG1, MP, LJ, HJ, CO),
            arrayOf(BTN, SB, BB, UTG, UTG1, MP, MP1, LJ, HJ, CO)
        )
        operator fun invoke(input: String): Position = when (input.replace(" ", "_").uppercase()) {
            "BTN" -> BTN
            "SB" -> SB
            "BB" -> BB
            "BUTTON" -> BTN
            "SMALL_BLIND" -> SB
            "BIG_BLIND" -> BB
            "CO" -> CO
            else -> NONE
        }

        operator fun invoke(size: Int, seat: Int): Position = table[size][seat]

    }
}
