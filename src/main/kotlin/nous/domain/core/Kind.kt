package nous.domain.core

enum class Kind : Entity {
    FIXLIMIT_HOLDEM,
    POTLIMIT_HOLDEM,
    NOLIMIT_HOLDEM,
    FIXLIMIT_OMAHA,
    POTLIMIT_OMAHA,
    NOLIMIT_OMAHA,
    NONE;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String = when(room) {
        Room.POKERSTARS -> when (this) {
            NOLIMIT_HOLDEM -> "Hold\'em No Limit"
            else -> ""
        }
        Room.POKER888 -> when (this) {
            NOLIMIT_HOLDEM -> "No Limit Holdem"
            else -> ""
        }
        else -> name.lowercase()
    }

    companion object {
        operator fun invoke(input: String) = when (input.uppercase().trim().replace("\'", "").replace(" ", "")) {
            "FIXLIMITHOLDEM" -> FIXLIMIT_HOLDEM
            "HOLDEMLIMIT" -> FIXLIMIT_HOLDEM
            "NOLIMITHOLDEM" -> NOLIMIT_HOLDEM
            "HOLDEMNOLIMIT" -> NOLIMIT_HOLDEM
            "TEXASHOLDEMNL" -> NOLIMIT_HOLDEM
            "POTLIMITHOLDEM" -> POTLIMIT_HOLDEM
            "POTLIMITOMAHA" -> POTLIMIT_OMAHA
            "OMAHAPOTLIMIT" -> POTLIMIT_OMAHA
            "FIXEDLIMITOMAHA" -> FIXLIMIT_OMAHA
            "NOLIMITOMAHA" -> NOLIMIT_OMAHA
            else -> NOLIMIT_HOLDEM
        }
    }
}
