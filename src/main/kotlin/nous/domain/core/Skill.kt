package nous.domain.core

enum class Skill {
    NONE,
    FISH,
    REG,
    ADV;
}
