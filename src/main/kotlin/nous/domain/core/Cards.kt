package nous.domain.core

import nous.utils.fmt.*
import nous.utils.math.*


fun allHands(m: Int, n: Int): ArrayList<Cards> {
    return allCombs(m, n).map { Cards(it) }.toCollection(ArrayList())
}

fun rndHands(k: Int, m: Int, n: Int): ArrayList<Cards> {
    return rndCombs(k, m, n).map { Cards(it) }.toCollection(ArrayList())
}

fun deal(sizes: Array<Int>): Array<String?> {
    return sizes.map { Deck.pick(it).toString() }.toTypedArray()
}

fun deal(size: Int): String {
    return Deck.pick(size).toString()
}


class Cards(private val elements: Array<Card>) : Entity {

    constructor(xs: IntArray):
        this(Array(xs.size) {i -> Card(xs[i]) })

    val size: Int get() = elements.size

    fun toInts(): IntArray = IntArray(elements.size) { i -> elements[i].toInt() }

    override fun toString(): String = elements.joinToString("")

    override fun toString(room: Room): String = when(room) {
        Room.POKERSTARS -> elements.joinToString(" ")
        Room.POKER888 -> elements.joinToString(", ")
        else -> elements.joinToString("")
    }

    companion object {
        private val separators: List<String> = listOf(", ", " ")

        operator fun invoke(input: String = ""): Cards = run {
            input.replace(separators.associateWith { "" }).let {
                Cards(it
                    .uppers()
                    .zip(it.lowers())
                    .map { (r, s) -> Card(Rank(r), Suit(s)) }
                    .toTypedArray()
                )
            }
        }

        fun all(m: Int): Sequence<Cards> = sequence {
            for (hand in allHands(m, Deck.NUM_CARDS))
                yield(hand)
        }

        fun one(k: Int, m: Int): Sequence<Cards> =
        sequence {
            for (hand in rndHands(k, m, Deck.NUM_CARDS))
                yield(hand)
        }
    }
}

// import java.util.BitSet
//    private var bitSet: BitSet = BitSet(Deck.NUM_CARDS)
//    constructor(cards: Array<Card>) {
//        bitSet = BitSet(Deck.NUM_CARDS)
//        for (card in cards) {
//            bitSet.set(card.ordinal)
//        }
//    }
//
//    fun reset(cards: IntArray) {
//        bitSet = BitSet(Deck.NUM_CARDS)
//        for (i in cards.indices) {
//            bitSet.set(cards[i])
//        }
//    }
//
//    fun addCard(card: Int) {
//        bitSet.set(card)
//    }
//
//    operator fun plus(hand: Hand): Hand {
//        bitSet.or(hand.bitSet)
//        return this
//    }
//
//    operator fun plus(cards: Array<Card>): Hand {
//        for (card in cards) {
//            bitSet.set(card.ordinal)
//        }
//        return this
//    }
//
//    operator fun minus(hand: Hand): Hand {
//        bitSet.andNot(hand.bitSet)
//        return this
//    }
//
//    operator fun minus(cards: Array<Card>): Hand {
//        for (card in cards) {
//            bitSet.clear(card.ordinal)
//        }
//        return this
//    }
//
//    fun toCards(): Array<Card?> {
//        val noCards = noCards()
//        val result = cardsOfNulls<Card>(noCards)
//        var j = -1
//        for (i in result.indices) {
//            j = bitSet.nextSetBit(j + 1)
//            result[i] = Card.values().get(j)
//        }
//        return result
//    }
//
//    fun noCards(): Int {
//        return bitSet.cardinality()
//    }
//
//    fun intersects(hand: Hand): Boolean {
//        return bitSet.intersects(hand.bitSet)
//    }
//
//    operator fun plus(card: Card): Hand {
//        bitSet.set(card.ordinal)
//        return this
//    }
//
//    operator fun minus(card: Card): Hand {
//        bitSet.clear(card.ordinal)
//        return this
//    }
//
//    override fun toString(): String {
//        return Card.toString(toCards())
//    }
//
//    fun reMoveRandom(): Card {
//        var cardNo: Int
//        do {
//            cardNo = random.nextInt(52)
//        } while (!bitSet[cardNo])
//        bitSet.clear(cardNo)
//        return Card.values().get(cardNo)
//    }
//
//    fun clear(): Hand {
//        bitSet.clear()
//        return this
//    }
//
//    fun intersects(p: Pair): Boolean {
//        val cards: Array<Card> = p.getCards()
//        if (this.contains(cards[0])) return true
//        return if (this.contains(cards[1])) true else false
//    }
//
//    private operator fun contains(card: Card): Boolean {
//        return bitSet[card.ordinal]
//    }
//
//    operator fun minus(p: Pair): Hand {
//        val cards: Array<Card> = p.getCards()
//        this.minus(cards)
//        return this
//    }
//
//    operator fun plus(p: Pair): Hand {
//        val cards: Array<Card> = p.getCards()
//        this.plus(cards)
//        return this
//    }
