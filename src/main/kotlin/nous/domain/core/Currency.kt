package nous.domain.core

enum class Currency : Entity {
    EUR,
    GBP,
    KZT,
    RUB,
    UAH,
    USD,
    NONE;

    override fun toString(room: Room): String =
        when(room) {
            Room.POKERSTARS -> " $name"
            Room.POKER888 -> when (this) {
                USD -> " USD"
                EUR -> "\\u20AC"
                else -> " $name"
            }
        else -> name
    }

    companion object {
        operator fun invoke(input: String): Currency = when (input) {
            "EUR" -> EUR
            "GBP" -> GBP
            "KZT" -> KZT
            "RUB" -> RUB
            "UAH" -> UAH
            else -> USD
        }
    }
}
