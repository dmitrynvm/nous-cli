package nous.domain.core

enum class Rank(private val element: Int) {
    TWO(0),
    THREE(1),
    FOUR(2),
    FIVE(3),
    SIX(4),
    SEVEN(5),
    EIGHT(6),
    NINE(7),
    TEN(8),
    JACK(9),
    QUEEN(10),
    KING(11),
    ACE(12),
    NONE(0);

    fun toInt(): Int = element

    override fun toString(): String = when (element) {
        0 -> "2"
        1 -> "3"
        2 -> "4"
        3 -> "5"
        4 -> "6"
        5 -> "7"
        6 -> "8"
        7 -> "9"
        8 -> "t"
        9 -> "j"
        10 -> "q"
        11 -> "k"
        12 -> "a"
        else -> "x"
    }.uppercase()

    companion object {
        fun all(): Sequence<Rank> = sequence {
            for (i in 0 until Deck.NUM_RANKS)
                yield(values()[i])
        }

        fun one(): Sequence<Rank> = sequence {
            values()[(0 until Deck.NUM_RANKS).random()]
        }

        operator fun invoke(n: Int): Rank = when (n) {
            0 -> TWO
            1 -> THREE
            2 -> FOUR
            3 -> FIVE
            4 -> SIX
            5 -> SEVEN
            6 -> EIGHT
            7 -> NINE
            8 -> TEN
            9 -> JACK
            10 -> QUEEN
            11 -> KING
            12 -> ACE
            else -> NONE
        }

        operator fun invoke(c: Char): Rank = when (c.lowercaseChar()) {
            '2' -> TWO
            '3' -> THREE
            '4' -> FOUR
            '5' -> FIVE
            '6' -> SIX
            '7' -> SEVEN
            '8' -> EIGHT
            '9' -> NINE
            't' -> TEN
            'j' -> JACK
            'q' -> QUEEN
            'k' -> KING
            'a' -> ACE
            else -> NONE
        }

        operator fun invoke(s: String): Rank = when (s.lowercase()) {
            "2" -> TWO
            "3" -> THREE
            "4" -> FOUR
            "5" -> FIVE
            "6" -> SIX
            "7" -> SEVEN
            "8" -> EIGHT
            "9" -> NINE
            "t" -> TEN
            "j" -> JACK
            "q" -> QUEEN
            "k" -> KING
            "a" -> ACE
            else -> NONE
        }
    }
}
