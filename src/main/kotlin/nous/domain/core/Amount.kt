package nous.domain.core

import kotlin.math.floor

class Amount(
    private val element: Double = 0.0,
    private val currency: Currency = Currency.EUR
) : Entity {

    operator fun compareTo(other: Int): Int = element.compareTo(other)

    operator fun compareTo(other: Double): Int = element.compareTo(other)

    operator fun compareTo(other: Amount): Int = element.compareTo(other.element)

    operator fun plus(other: Amount): Amount = Amount(element + other.element)

    operator fun minus(other: Amount): Amount = Amount(element - other.element)

    fun toDouble(): Double = element

    override fun toString(): String = element.toString()

    override fun toString(room: Room): String = run {
        val elem = if(element == floor(element)) element.toInt().toString() else element.toString()
        when (room) {
            Room.POKERSTARS -> "\$$elem"
            Room.POKER888 -> "\$$elem"
            else -> elem.lowercase()
        }
    }

    companion object {
        operator fun invoke(input: String): Amount =
            if(input.isNotBlank())
                if(',' in input)
                    if('.' in input)
                        Amount(input.replace(",", "").toDouble() )
                    else
                        Amount(input.replace(",", ".").toDouble() )
                else
                    Amount(input.toDouble() )
            else
                Amount()
    }
}
