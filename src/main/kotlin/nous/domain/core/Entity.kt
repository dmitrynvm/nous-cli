package nous.domain.core

interface Entity {
    fun toString(room: Room): String
}