package nous.domain.core

enum class Side : Entity {
    MAIN,
    FIRST,
    SECOND,
    THIRD;

    override fun toString(): String = name

    override fun toString(room: Room): String = when(room) {
        Room.POKERSTARS -> when(this) {
            MAIN -> name
            else -> ""
        }
        else -> name.lowercase()
    }

    companion object {
        operator fun invoke(input: String): Side = when (input.trim().uppercase()) {
            "FIRST"     -> FIRST
            "SECOND"    -> SECOND
            "THIRD"     -> THIRD
            else        -> MAIN
        }
    }
}
