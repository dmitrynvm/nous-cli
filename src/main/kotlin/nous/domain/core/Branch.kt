package nous.domain.core

enum class Branch : Entity {
    NONE,
    FIRST,
    SECOND,
    THIRD;

    fun isNotEmpty(): Boolean = this != NONE

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String = when(room) {
        Room.POKERSTARS -> when(this) {
            NONE -> name
            else -> ""
        }
        else -> name
    }

    companion object {
        operator fun invoke(input: String): Branch = when (input.trim().uppercase()) {
            "FIRST"     -> FIRST
            "SECOND"    -> SECOND
            "THIRD"     -> THIRD
            else        -> NONE
        }
    }
}
