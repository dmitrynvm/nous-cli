package nous.domain.core

enum class Speed : Entity {
    FAST,
    NORMAL;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String = when(room) {
        Room.POKERSTARS -> when (this) {
            NORMAL -> ""
            FAST -> " ZOOM"
        }
        Room.POKER888 -> when (this) {
            NORMAL -> ""
            FAST -> " SNAP"
        }
        else -> name.lowercase()
    }

    companion object {
        operator fun invoke(input: String) = when (input.uppercase().trim()) {
            "ZOOM"  -> FAST
            "SNAP"  -> FAST
            else    -> NORMAL
        }
    }
}
