package nous.domain.core

enum class Bonus : Entity {
    BINGO,
    BINGO_JACKPOT_RAKE,
    JACKPOT,
    JACKPOT_RAKE,
    RAKE,
    NONE;

    override fun toString(): String = name.lowercase()
    override fun toString(room: Room): String = name.lowercase()

    companion object {
        operator fun invoke(input: String): Bonus = when (input.replace(" ", "_").uppercase()) {
            "JACKPOT_TABLE" -> JACKPOT_RAKE
            else            -> RAKE
        }
    }
}
