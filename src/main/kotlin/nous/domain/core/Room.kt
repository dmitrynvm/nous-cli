package nous.domain.core

enum class Room : Entity {
    POKERGG,
    POKERPARTY,
    POKER888,
    POKERBH3,
    POKERDOM,
    POKERMATE,
    POKERSTARS,
    POKERPP,
    NONE;

    override fun toString(): String = name.lowercase()

    override fun toString(room: Room): String =
        when(room) {
            POKERSTARS -> "PokerStars"
            POKER888 -> "888poker"
            else -> name.lowercase()
        }

    companion object {
        operator fun invoke(input: String): Room = when (input.lowercase()) {
            "ggpokerok" -> POKERGG
            "888poker"  -> POKER888
            "wsop.com"  -> POKER888
            "cassava"   -> POKER888
            "lotospoker"-> POKER888
            "888.es"    -> POKER888
            "888.ro"    -> POKER888
            "888.it"    -> POKER888
            "poker888"  -> POKER888
            "pokerbh3"  -> POKERBH3
            "dompoker"  -> POKERDOM
            "pokermate" -> POKERMATE
            "pokerstars"-> POKERSTARS
            "pppoker"   -> POKERPP
            else        -> NONE
        }
    }
}
