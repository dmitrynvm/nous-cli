package nous.domain.core

enum class Suit(private val element: Int) {
    CLUBS(0),
    DIAMONDS(1),
    HEARTS(2),
    SPADES(3);

    fun toInt(): Int = element

    override fun toString(): String = when (element) {
        0 -> "c"
        1 -> "d"
        2 -> "h"
        else -> "s"
    }

    companion object {
        operator fun invoke(n: Int): Suit = when (n) {
            0 -> CLUBS
            1 -> DIAMONDS
            2 -> HEARTS
            else -> SPADES
        }

        operator fun invoke(c: Char): Suit = when (c) {
            'c' -> CLUBS
            'd' -> DIAMONDS
            'h' -> HEARTS
            else -> SPADES
        }

        operator fun invoke(s: String): Suit = when (s) {
            "c" -> CLUBS
            "d" -> DIAMONDS
            "h" -> HEARTS
            else -> SPADES
        }

        fun all(): Sequence<Suit> = sequence {
            for (i in 0 until Deck.NUM_SUITS)
                yield(values()[i])
        }

        fun any(): Sequence<Suit> = sequence {
            values()[(0 until Deck.NUM_SUITS).random()]
        }
    }
}
