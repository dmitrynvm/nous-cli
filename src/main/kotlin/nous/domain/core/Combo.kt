package nous.domain.core

enum class Combo : Entity {
    STRAIGHT_FLUSH,
    FOUR_OF_A_KIND,
    FULL_HOUSE,
    FLUSH,
    STRAIGHT,
    THREE_OF_A_KIND,
    TWO_PAIR,
    ONE_PAIR,
    HIGH_CARD;

    override fun toString(room: Room): String = name.lowercase()

    companion object {
        operator fun invoke(input: String): Combo = run {
            STRAIGHT_FLUSH
        }
    }
}
