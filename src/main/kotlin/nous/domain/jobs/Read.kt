package nous.domain.jobs

import nous.domain.core.*
import nous.domain.event.*
import nous.framework.*
import nous.utils.io.*
import me.tongfei.progressbar.ProgressBar
import nous.domain.config.ParseConfig
import kotlin.streams.asSequence

open class Read(
    private val source: Room,
) {
    private val parse = Parse(source)
    private val select = Select()
    private val config: ParseConfig = fromYaml(pwd("jobs/parse/$source.yml"))

    operator fun invoke(input: String, progress: ProgressBar? = null): List<List<Pair<String, Result<List<HoldemEvent>>>>> =
        input
            .files()
            .parallelStream()
            .map { file ->
                progress?.step()
                file
                    .read()
                    .hands(config.separator.hand)
                    .parallelStream()
                    .map { hand ->
                        hand to select(
                            parse(
                                hand.lines(config.separator.line)
                            )
                        )
                    }.asSequence().toList()
    }.asSequence().toList()
}
