package nous.domain.jobs

import nous.domain.core.*
import nous.utils.fmt.*
import nous.utils.io.*
import me.tongfei.progressbar.ProgressBar
import nous.domain.event.*

data class Row(
    val numPlayers: Int,
    val ordinal: Int,
    val position: String,
    val cards: String,
    val equity: Double,
    val first: String,
    val second: String,
    val third: String,
    val fourth: String,
    val fifth: String,
    val action: String
)

open class Extract(
    private val source: Room,
    private val target: Room = Room.NONE
) {
    private val read = Read(source)
    private val rotate = Rotate()
    private val select = Select()
    private val obfuscate = Obfuscate()
    private val dump = Dump(target)
    private val equity = mapOf(
        "AAo" to 0.85217,
        "KKo" to 0.823355,
        "QQo" to 0.798325,
        "JJo" to 0.77494,
        "TTo" to 0.750225,
        "99o" to 0.72123,
        "88o" to 0.69152,
        "AKs" to 0.670985,
        "AQs" to 0.66045,
        "77o" to 0.660275,
        "AKo" to 0.65318,
        "AJs" to 0.650795,
        "ATs" to 0.64696,
        "AQo" to 0.643145,
        "AJo" to 0.63616,
        "66o" to 0.6357,
        "KQs" to 0.634845,
        "A9s" to 0.62956,
        "ATo" to 0.62688,
        "KJs" to 0.625955,
        "A8s" to 0.620465,
        "KTs" to 0.61856,
        "KQo" to 0.614135,
        "A9o" to 0.60859,
        "KJo" to 0.607925,
        "A7s" to 0.607095,
        "55o" to 0.602495,
        "QJs" to 0.601045,
        "A6s" to 0.600355,
        "K9s" to 0.59902,
        "A5s" to 0.598365,
        "A8o" to 0.598075,
        "QTs" to 0.596065,
        "KTo" to 0.59585,
        "A4s" to 0.588765,
        "A7o" to 0.5887,
        "QJo" to 0.584125,
        "A3s" to 0.58307,
        "K8s" to 0.58182,
        "A5o" to 0.57742,
        "A6o" to 0.577345,
        "K9o" to 0.57703,
        "Q9s" to 0.576365,
        "K7s" to 0.576335,
        "JTs" to 0.575415,
        "A2s" to 0.574375,
        "QTo" to 0.57384,
        "44o" to 0.56797,
        "A4o" to 0.5678,
        "K6s" to 0.56706,
        "K8o" to 0.560095,
        "A3o" to 0.559765,
        "Q8s" to 0.559755,
        "K5s" to 0.558975,
        "Q9o" to 0.55496,
        "JTo" to 0.554295,
        "J9s" to 0.55383,
        "K7o" to 0.55022,
        "A2o" to 0.548555,
        "K4s" to 0.547795,
        "K3s" to 0.540695,
        "K6o" to 0.54044,
        "Q7s" to 0.54035,
        "J8s" to 0.54026,
        "33o" to 0.53893,
        "T9s" to 0.53869,
        "Q6s" to 0.53749,
        "Q8o" to 0.5355,
        "K5o" to 0.53265,
        "J9o" to 0.53194,
        "K2s" to 0.52917,
        "J7s" to 0.524615,
        "Q5s" to 0.524465,
        "K4o" to 0.524095,
        "T8s" to 0.522095,
        "Q7o" to 0.517645,
        "J8o" to 0.51634,
        "Q4s" to 0.516255,
        "T9o" to 0.51587,
        "K3o" to 0.513225,
        "Q6o" to 0.511665,
        "Q3s" to 0.510095,
        "98s" to 0.50663,
        "T7s" to 0.50618,
        "22o" to 0.505325,
        "J6s" to 0.503965,
        "K2o" to 0.50386,
        "Q5o" to 0.503155,
        "Q2s" to 0.502275,
        "J5s" to 0.499425,
        "T8o" to 0.49874,
        "J7o" to 0.49501,
        "Q4o" to 0.491265,
        "97s" to 0.491115,
        "T6s" to 0.49054,
        "J4s" to 0.490155,
        "Q3o" to 0.48118,
        "98o" to 0.481145,
        "J3s" to 0.479645,
        "T7o" to 0.478745,
        "87s" to 0.47719,
        "J6o" to 0.476635,
        "96s" to 0.47514,
        "J2s" to 0.473095,
        "T5s" to 0.472595,
        "Q2o" to 0.47251,
        "J5o" to 0.471315,
        "T4s" to 0.4654,
        "86s" to 0.46428,
        "J4o" to 0.463065,
        "97o" to 0.46233,
        "T6o" to 0.461185,
        "T3s" to 0.45761,
        "76s" to 0.45492,
        "95s" to 0.454575,
        "J3o" to 0.453405,
        "87o" to 0.44938,
        "T2s" to 0.44854,
        "96o" to 0.446735,
        "85s" to 0.44557,
        "J2o" to 0.443295,
        "T5o" to 0.44253,
        "94s" to 0.43814,
        "T4o" to 0.43639,
        "75s" to 0.43588,
        "93s" to 0.434325,
        "86o" to 0.43306,
        "65s" to 0.431595,
        "T3o" to 0.426105,
        "95o" to 0.4256,
        "84s" to 0.42533,
        "92s" to 0.423895,
        "76o" to 0.42029,
        "74s" to 0.41758,
        "54s" to 0.416035,
        "64s" to 0.41495,
        "T2o" to 0.41377,
        "85o" to 0.41231,
        "83s" to 0.408155,
        "94o" to 0.40608,
        "75o" to 0.404475,
        "73s" to 0.402615,
        "82s" to 0.40125,
        "93o" to 0.400105,
        "63s" to 0.397065,
        "65o" to 0.39684,
        "53s" to 0.39637,
        "84o" to 0.393885,
        "43s" to 0.388395,
        "92o" to 0.388295,
        "74o" to 0.384745,
        "54o" to 0.38307,
        "72s" to 0.380055,
        "52s" to 0.37955,
        "64o" to 0.377735,
        "62s" to 0.37631,
        "83o" to 0.3746,
        "42s" to 0.36973,
        "82o" to 0.36764,
        "73o" to 0.367275,
        "63o" to 0.36134,
        "53o" to 0.36119,
        "32s" to 0.35792,
        "43o" to 0.35172,
        "72o" to 0.346965,
        "52o" to 0.343785,
        "62o" to 0.339745,
        "42o" to 0.33209,
        "32o" to 0.3241,
    )

    fun get_equity(key: String): Double {
        return if(key in equity)
            equity[key]!!
        else {
            equity[key[1].toString() + key[0].toString() + key[2].toString()]!!
        }
    }

    operator fun invoke(input: String, output: String, progress: ProgressBar? = null) = run {
        mkdir(output)
        val failure = join(output, "failure.txt").open()
//        val scrapped = join(output, "scrapped.json").open()
        val success = join(output, "success.txt").open()
        success.write("num_players,ordinal,position,cards,equity,first,second,third,fourth,fifth,action")
        read(input)
            .forEach { file ->
            file
                .forEach { (hand, parsed) ->
                progress?.step()
                val processed = rotate(select(parsed))
                processed.mapBoth(
                    {
                        failure.write(hand)
                    },
                    {
                        val hero = it.filterIsInstance<HeroEvent>()
                        if(hero.isNotEmpty()) {
                            val h = hero.first()
                            val players = it.filterIsInstance<PlayerEvent>()
                            val stakes = it.filterIsInstance<StakeEvent>()
                                .filter { ev -> ev !is SmallBlindEvent && ev !is BigBlindEvent && ev !is DeadBlindEvent}
                            val numPlayers = players.size
                            val end = stakes.indexOfFirst { ev -> ev.name == h.name }
                            val before = stakes.slice(0 until end)
                            if (before.isNotEmpty()) {
                                val last = stakes[end]
                                val cards = h.cards.toString()
                                val a = cards[0].toString()
                                val aa = cards[1].toString()
                                val b = cards[2].toString()
                                val bb = cards[3].toString()
                                val key = a + b + if (aa == bb) "s" else "o"
                                val first = before.getOrElse(0) { NoneEvent() }.type
                                val second = before.getOrElse(1) { NoneEvent() }.type
                                val third = before.getOrElse(2) { NoneEvent() }.type
                                val fourth = before.getOrElse(3) { NoneEvent() }.type
                                val fifth = before.getOrElse(4) { NoneEvent() }.type
                                val action = last.type
                                success.write(
                                    "$numPlayers,${before.size + 1},${h.position},${h.cards},${get_equity(key)},$first,$second,$third,$fourth,$fifth,$action"
                                )
                            }
                        }
                    }
                )
            }
        }
        progress?.stepTo(progress.max)
    }
}
