package nous.domain.jobs

import nous.domain.core.*
import nous.domain.event.*
import nous.framework.*

class Convert(
    private val source: Room,
    private val target: Room
) /*: Job<List<Result<HoldemEvent>>, List<Result<HoldemEvent>>>*/ {

    operator fun invoke(input: List<Result<HoldemEvent>>): List<List<HoldemEvent>> = run {
        val events: List<HoldemEvent> = input.getOrElse( NoneEvent() )
        val outputs: MutableList<MutableList<HoldemEvent>> = mutableListOf()
        val heroes = events.filterIsInstance<HeroEvent>()
        val dealer = events.filterIsInstance<DealerEvent>().first()
//        val total = events.filterIsInstance<TotalEvent>().first()
        for(hero in heroes) {
            val output: MutableList<HoldemEvent> = mutableListOf()
            for (event in events) {
                if (event is TitleEvent) {
                    val header = HeaderEvent(
                        room = event.room,
                        name = event.name,
                        speed = event.speed,
                        bonus = event.bonus
                    )
                    val footer = FooterEvent(
                        sb = event.sb,
                        bb = event.bb,
                        kind = event.kind,
                        date = event.date,
                        zone = event.zone
                    )
                    val button = ButtonEvent(seat = dealer.seat)
                    val table = TableEvent(size = dealer.size)
//                    val take = TakeEvent(
//                        name = total
//                        amount = total.amount
//                    )
                    output.add(header)
                    output.add(footer)
                    output.add(hero)
                    output.add(button)
                    output.add(table)
//                    output.add(take)
                } else if(event is HeroEvent || event is DealingEvent || event is TotalEvent || event is BoardEvent || event is UncallEvent || event is TotalEvent) {
                    continue
                } else {
                    output.add(event)
                }
            }
            outputs.add(output)
        }
        outputs
    }
}

fun main() {
    val parse = Parse(Room.POKERBH3)
    val convert = Convert(Room.POKERBH3, Room.POKER888)
    val dump = Dump(Room.POKER888)
    val input = """
        #221373500: Texas Hold'em NL (4 р./8 р. RUB) - 2016/10/20 09:57:42 UTC
        Table 'Dubna Jackpot [ 40BB / 100BB ] 9Max #3324' Seat #3 is the button
        Seat 3: 54102bfd89051d144998268e (693.76 р. in chips)
        Seat 4: 5699170a6d9dc97bdd809152 (627.60 р. in chips)
        Seat 5: 57571aa2190eb9a425d032e7 (275.81 р. in chips)
        Seat 6: 55efd05a6d9dc97bdd37c78a (1,247.34 р. in chips)
        Seat 7: 53ebbfe389051d144990482a (814.47 р. in chips)
        Seat 8: 553d537d6d9dc97bddd778df (427.12 р. in chips)
        Seat 0: 57ffa05d50be63e9f4ccab03 (493.94 р. in chips)
        Seat 1: 55c1ad3f6d9dc97bddd1c259 (352 р. in chips)
        Seat 2: 56e89115190eb9a4253e288b (788 р. in chips)
        5699170a6d9dc97bdd809152: posts small blind 4 р.
        57571aa2190eb9a425d032e7: posts big blind 8 р.
        *** HOLE CARDS ***
        54102bfd89051d144998268e: [Ts 7s]
        5699170a6d9dc97bdd809152: [2h 4s]
        57571aa2190eb9a425d032e7: [4h 9h]
        55efd05a6d9dc97bdd37c78a: [6h Qc]
        53ebbfe389051d144990482a: [Kc 5s]
        553d537d6d9dc97bddd778df: [7h Qh]
        57ffa05d50be63e9f4ccab03: [6s 9s]
        55c1ad3f6d9dc97bddd1c259: [4d 5h]
        56e89115190eb9a4253e288b: [3s Ad]
        *** PREFLOP ***
        55efd05a6d9dc97bdd37c78a: folds
        53ebbfe389051d144990482a: folds
        553d537d6d9dc97bddd778df: calls 8 р.
        57ffa05d50be63e9f4ccab03: calls 8 р.
        55c1ad3f6d9dc97bddd1c259: folds
        56e89115190eb9a4253e288b: folds
        54102bfd89051d144998268e: calls 8 р.
        5699170a6d9dc97bdd809152: folds
        57571aa2190eb9a425d032e7: checks
        *** FLOP *** [9d 3d Kh]
        57571aa2190eb9a425d032e7: checks
        553d537d6d9dc97bddd778df: checks
        57ffa05d50be63e9f4ccab03: checks
        54102bfd89051d144998268e: checks
        *** TURN *** [9d 3d Kh] [Ks]
        57571aa2190eb9a425d032e7: bets 8 р.
        553d537d6d9dc97bddd778df: folds
        57ffa05d50be63e9f4ccab03: folds
        54102bfd89051d144998268e: folds
        57571aa2190eb9a425d032e7: uncalled bet 8 р.
        57571aa2190eb9a425d032e7 collected 33.89 р. from main pot
        *** SUMMARY ***
        Total pot 36 р.. | Rake 1.75 р.. Jackpot fee 0.36 р..
        Board [9d 3d Kh Ks]
    """.trimIndent()
    val parsed = parse(input.lines())
//    val converted = convert(parsed)

//    for(events in converted) {
//        val results: List<Result<HoldemEvent>> = events.map { it.toRight() }
//        val cleaned = clean(results)
//        val dumped = dump(cleaned)
//        println(dumped.join())
//    }
}
