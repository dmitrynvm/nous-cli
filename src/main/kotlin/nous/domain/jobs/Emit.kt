package nous.domain.jobs

import nous.domain.config.ParseConfig
import nous.domain.event.*
import nous.utils.fmt.*
import java.util.regex.Matcher
import kotlin.reflect.KProperty1

typealias Emitter = (Matcher) -> (HoldemEvent)

object Emit {

    //---------------------------------< INIT >-------------------------------//
    val title: Emitter = {
        TitleEvent(
            name    = it.getOrElse("name").lowercase(),
            kind    = it.getOrElse("kind").toType(),
            speed   = it.getOrElse("speed").toSpeed(),
            sb      = it.getOrElse("sb").toAmount(),
            bb      = it.getOrElse("bb").toAmount(),
            currency= it.getOrElse("currency").toCurrency(),
            date    = it.getOrElse("date").toDate(),
            bonus   = it.getOrElse("bonus").toBonus(),
            zone    = it.getOrElse("zone").toZone()
        )
    }
    val header: Emitter = {
        HeaderEvent(
            room    = it.getOrElse("room").toRoom(),
            name    = it.getOrElse("name").lowercase(),
            speed   = it.getOrElse("speed").toSpeed(),
            bonus   = it.getOrElse("bonus").toBonus()
        )
    }
    val footer: Emitter = {
        FooterEvent(
            sb      = it.getOrElse("sb").toAmount(),
            bb      = it.getOrElse("bb").toAmount(),
            kind    = it.getOrElse("kind").toType(),
            date    = it.getOrElse("date").toDate(),
        )
    }
    val table: Emitter = {
        TableEvent(
            size    = it.getOrElse("size").toInteger()
        )
    }
    val button: Emitter = {
        ButtonEvent(
            seat    = it.getOrElse("seat").toInteger()
        )
    }
    val hero: Emitter = {
        HeroEvent(
            name    = it.getOrElse("name").lowercase(),
            cards   = it.getOrElse("cards").toCards()
        )
    }
    val dealer: Emitter = {
        DealerEvent(
            name    = it.getOrElse("name").lowercase(),
            size    = it.getOrElse("size").toInteger(),
            mode   = it.getOrElse("mode").toMode()
        )
    }
    val player: Emitter = {
        PlayerEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
            stack   = it.getOrElse("stack").toAmount(),
            position= it.getOrElse("position").toPosition()
        )
    }

    //---------------------------------< MOVE >-------------------------------//
    val seating: Emitter = {
        SeatingEvent()
    }
    val posting: Emitter = {
        PostingEvent()
    }
    val dealing: Emitter = {
        DealingEvent()
    }
    val preflop: Emitter = {
        PreflopEvent()
    }
    val flop: Emitter = {
        FlopEvent(
            cards   = it.getOrElse("cards").toCards(),
            branch  = it.getOrElse("branch").toBranch()
        )
    }
    val turn: Emitter = {
        TurnEvent(
            cards   = it.getOrElse("cards").toCards(),
            more    = it.getOrElse("more").toCards(),
            branch  = it.getOrElse("branch").toBranch()
        )
    }
    val river: Emitter = {
        RiverEvent(
            cards   = it.getOrElse("cards").toCards(),
            more    = it.getOrElse("more").toCards(),
            branch  = it.getOrElse("branch").toBranch()
        )
    }
    val showdown: Emitter = {
        ShowdownEvent(
            branch  = it.getOrElse("branch").toBranch()
        )
    }
    val summary: Emitter = {
        SummaryEvent()
    }
    val fork: Emitter = {
        ForkEvent()
    }

    //---------------------------------< STAKE >-------------------------------//
    val ante: Emitter = {
        AnteEvent(
            name    = it.getOrElse("name").lowercase(),
            amount = it.getOrElse("amount").toAmount()
        )
    }
    val smallBlind: Emitter = {
        SmallBlindEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val bigBlind: Emitter = {
        BigBlindEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val straddle: Emitter = {
        StraddleEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val striddle: Emitter = {
        StriddleEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val deadBlind: Emitter = {
        DeadBlindEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val twinBlind: Emitter = {
        DeadBlindEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount() + it.getOrElse("other").toAmount()
        )
    }
    val fold: Emitter = {
        FoldEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val foldShow: Emitter = {
        FoldShowEvent(
            name    = it.getOrElse("name").lowercase(),
            cards   = it.getOrElse("cards").toCards()
        )
    }
    val check: Emitter = {
        CheckEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val bet: Emitter = {
        BetEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val call: Emitter = {
        CallEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val raise: Emitter = {
        RaiseEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount(),
            other    = it.getOrElse("other").toAmount()
        )
    }
    val push: Emitter = {
        PushEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val insure: Emitter = {
        InsureEvent(
            seat    = it.getOrElse("seat").toInteger()
        )
    }

    //-------------------------------< SHOWDOWN >-----------------------------//
    val hide: Emitter = {
        HideEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val muck: Emitter = {
        MuckEvent(
            name    = it.getOrElse("name").lowercase(),
            cards   = it.getOrElse("cards").toCards()
        )
    }
    val show: Emitter = {
        ShowEvent(
            name    = it.getOrElse("name").lowercase(),
            cards   = it.getOrElse("cards").toCards(),
            combo   = it.getOrElse("combo").toCombo()
        )
    }
    val uncall: Emitter = {
        UncallEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount()
        )
    }
    val take: Emitter = {
        TakeEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount(),
            side    = it.getOrElse("side").toInteger(),
            players = it.getOrElse("players")
        )
    }
    val cashout: Emitter = {
        CashoutEvent(
            name    = it.getOrElse("name").lowercase(),
            amount  = it.getOrElse("amount").toAmount(),
            fee     = it.getOrElse("fee").toAmount()
        )
    }

    //--------------------------------< SUMMARY >------------------------------//
    val total: Emitter = {
        TotalEvent(
            amount  = it.getOrElse("amount", "0.0").toAmount(),
            rake    = it.getOrElse("rake", "0.0").toAmount(),
            jack    = it.getOrElse("jack", "0.0").toAmount(),
            bingo   = it.getOrElse("bingo", "0.0").toAmount()
        )
    }
    val board: Emitter = {
        BoardEvent(
            cards   = it.getOrElse("cards").toCards(),
            branch  = it.getOrElse("branch").toBranch()
        )
    }
    val showed: Emitter = {
        ShowedEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
            cards   = it.getOrElse("cards").toCards()
        )
    }
    val win: Emitter = {
        WinEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
            cards   = it.getOrElse("cards").toCards(),
            pot     = it.getOrElse("pot").toAmount(),
            amount  = it.getOrElse("amount").toAmount(),
        )
    }
    val lose: Emitter = {
        LoseEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
            cards   = it.getOrElse("cards").toCards(),
            amount  = it.getOrElse("amount", "0.0").toAmount(),
        )
    }
    val hidden: Emitter = {
        MuckedEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
        )
    }
    val folded: Emitter = {
        FoldedEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger(),
            position= it.getOrElse("position").toPosition()
        )
    }
    val taken: Emitter = {
        TakenEvent(
            seat    = it.getOrElse("seat").toInteger()
        )
    }

    //--------------------------------< NETWORK >------------------------------//
    val say: Emitter = {
        SayEvent(
            name    = it.getOrElse("name").lowercase(),
            msg     = it.getOrElse("msg")
        )
    }
    val said: Emitter = {
        SaidEvent(
            name    = it.getOrElse("name").lowercase(),
            msg     = it.getOrElse("msg")
        )
    }
    val join: Emitter = {
        JoinEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger()
        )
    }
    val leave: Emitter = {
        LeaveEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val allow: Emitter = {
        AllowEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val fail: Emitter = {
        FailEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val connect: Emitter = {
        ConnectEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val disconn: Emitter = {
        DisconnEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val sitin: Emitter = {
        SitinEvent(
            name    = it.getOrElse("name").lowercase()
        )
    }
    val sitout: Emitter = {
        SitoutEvent(
            name    = it.getOrElse("name").lowercase(),
            seat    = it.getOrElse("seat").toInteger()
        )
    }
    val none: Emitter = {
        NoneEvent()
    }

    operator fun get(input: String): Emitter =
        (this::class.members.first { it.name == input } as KProperty1<Any, *>).get(this) as Emitter

    operator fun invoke(m: ParseConfig): List<Regexer> = m.pattern.toMap().map { (k, v) -> regex(Emit[k], v) }
}
