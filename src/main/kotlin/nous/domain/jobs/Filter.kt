package nous.domain.jobs

import nous.domain.core.*
import nous.utils.fmt.*
import nous.utils.io.*
import me.tongfei.progressbar.ProgressBar

open class Filter(
    private val source: Room,
    private val target: Room = Room.NONE
) {
    private val read = Read(source)
    private val obfuscate = Obfuscate()
    private val dump = Dump(target)

    operator fun invoke(input: String, output: String, progress: ProgressBar? = null) = run {
        val failure = join(output, "failure.txt").open()
//        val scrapped = join(output, "scrapped.json").open()
        val success = join(output, "success.txt").open()
        mkdir(output)
        read(input).forEach { file ->
            file.forEach { (hand, parsed) ->
                progress?.step()
                parsed.mapBoth(
                    {
                        failure.write(hand)
                    },
                    {
                        success.write(hand)
//                        scrapped.write(it.map { ev -> ev.toMap() }.toJson())
                    }
                )
            }
        }
        progress?.stepTo(progress.max)
    }
}
