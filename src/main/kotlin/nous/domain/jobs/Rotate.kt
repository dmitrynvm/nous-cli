package nous.domain.jobs

import nous.domain.core.*
import nous.domain.event.*
import nous.framework.*
import nous.utils.fmt.*
import nous.utils.io.*
import nous.utils.math.*

class Rotate() : Job<Result<List<HoldemEvent>>, Result<List<HoldemEvent>>> {
    override fun invoke(input: Result<List<HoldemEvent>>): Result<List<HoldemEvent>> =
        input.mapRight { events ->
            val players = events.filterIsInstance<PlayerEvent>().sortedBy { e -> e.seat }
            val seatsToSeats = (players.mapIndexed { i, e -> e.seat to i + 1 }).toMap()
            val namesToSeats = (players.mapIndexed { i, e -> e.name to i + 1 }).toMap()
            val seatsToNames = (players.mapIndexed { i, e -> i + 1 to e.name }).toMap()

            val log = join("log.txt").open()
//            log.write(namesToSeats.toJson())

            val button = events.filterIsInstance<ButtonEvent>().first().seat
            val seats = (players.map { it.seat }).toList()
            val closestIndex = (seats.indexOfFirst { it == seats.closest(button) } )
            val index = if(closestIndex < button) closestIndex else (closestIndex - 1).mod(seats.size)
            val bt = if(button in seatsToSeats) seatsToSeats[button]!! else index
            val size = events.filterIsInstance<TableEvent>().first().size

//            log.write(events.join())
//            log.write("--------------------------------")
//            log.write(seatsToSeats.toJson())
//            log.write(bt.toString())

            events
                .map {
                    when(it) {
                        is TitleEvent -> it.setName(it.name.lowercase())
                        is HeaderEvent -> it.setName(it.name.lowercase())
                        is DealerEvent -> it.setName(it.name.lowercase())
                        is ButtonEvent -> it.setSeat(bt)
                        is SecretEvent -> {
                            val seat = seatsToSeats[it.seat] ?: namesToSeats[it.name]!!
                            it.setData(it.name.lowercase(), seat, Position.NONE)
                        }
                        else -> it
                        }
                }
                .map {
                    when(it) {
                        is ButtonEvent -> it.setSeat(1)
                        is SecretEvent -> {
                            val seat = (it.seat - button + 1).mod(size) + 1
                            val position = Position(size-1, seat-1)
                            it.setData(it.name, seat, position)
                        }
                        else -> it
                    }
                }.sortedWith(Compare)
            }

    object Compare : Comparator<HoldemEvent> {
        override fun compare(a: HoldemEvent, b: HoldemEvent): Int =
            when {
                a is PlayerEvent && b is PlayerEvent -> a.seat - b.seat
                else -> a.ordinal - b.ordinal
            }
    }
}
