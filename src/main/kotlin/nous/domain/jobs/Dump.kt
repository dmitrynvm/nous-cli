package nous.domain.jobs

import nous.domain.core.*
import nous.domain.event.*
import nous.framework.*
import nous.utils.fmt.*

class Dump(private val room: Room) : Job<List<HoldemEvent>, String> {
    override fun invoke(input: List<HoldemEvent>): String =
        input.map { event ->
            buildTemplate(room)[
                    event::class
                        .simpleName
                        ?.replaceFirstChar { c -> c.lowercase() }
                        ?.replace("Event", "")
            ]?.render(event.toMap(room)) ?: ""
        }.join()
}
