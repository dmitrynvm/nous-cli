package nous.domain.jobs

import nous.domain.event.*
import nous.domain.error.*
import nous.framework.*
import nous.utils.fmt.*

class Select : Job<Result<List<HoldemEvent>>, Result<List<HoldemEvent>>> {
    override fun invoke(input: Result<List<HoldemEvent>>): Result<List<HoldemEvent>> = run {
        val events = input.getOrElse { emptyList() }
        val players = events.filterIsInstance<PlayerEvent>()
        if(players.count() <= 6)
            input
        else
            HandError(events.join()).toLeft()
    }
}
