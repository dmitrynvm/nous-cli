package nous.domain.jobs

import nous.domain.event.*
import nous.framework.*
import nous.utils.fmt.*

class Obfuscate : Job<Result<List<HoldemEvent>>, Result<List<HoldemEvent>>> {
    override fun invoke(input: Result<List<HoldemEvent>>): Result<List<HoldemEvent>> =
        input.mapRight { events ->
            val players = events.filterIsInstance<PlayerEvent>().sortedBy { e -> e.seat }
            val uuids = (players.map { e -> e.name to uuid(e.name) }).toMap()

            events.map { event ->
                when(event) {
                    is TitleEvent -> event.setName(event.name)
                    is HeaderEvent -> event.setName(event.name)
                    is DealerEvent -> event.setName(duid())
                    is SecretEvent -> event.setName(uuids[event.name]!!)
                    else -> event
                }
            }
        }
}
