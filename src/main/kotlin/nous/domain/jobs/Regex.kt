package nous.domain.jobs

import nous.domain.event.*
import nous.domain.error.*
import nous.framework.*

typealias Regexer = (String) -> (Result<HoldemEvent>)

fun regex(transform: Emitter, pattern: String): Regexer = { input ->
    val matcher = pattern.toPattern().matcher(input)
    if (matcher.find()) transform(matcher).toRight() else LineError(input).toLeft()
}
