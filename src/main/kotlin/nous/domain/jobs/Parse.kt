package nous.domain.jobs

import nous.domain.config.*
import nous.domain.core.*
import nous.domain.event.*
import nous.domain.error.*
import nous.framework.*
import nous.utils.fmt.*
import nous.utils.io.*

class Parse(
    private val source: Room
) : Job<List<String>, Result<List<HoldemEvent>>> {
    private val config: ParseConfig = fromYaml(pwd("jobs/parse/$source.yml"))
    private val parsers = Emit(config)

    operator fun invoke(input: String): Result<HoldemEvent> = choice(parsers)(input)

    override fun invoke(input: List<String>): Result<List<HoldemEvent>> = many(choice(parsers))(input)

    companion object {
        fun choice(parsers: List<Regexer>): (String) -> Result<HoldemEvent> = { input ->
            parsers.map { parser -> parser(input) }
                .firstOrNull { it.isRight() } ?: LineError(input).toLeft()
        }

        fun many(parser: (String) -> Result<HoldemEvent>): (List<String>) -> Result<List<HoldemEvent>> = { input: List<String> ->
            input.map { parser(it) }.let { results ->
                if (results.isRight()) {
                    val events: List<HoldemEvent> = results.getOrElse(NoneEvent())
                    events.toRight()
                } else
                    HandError(results.join()).toLeft()
            }
        }
    }
}
