package nous.domain.error

import nous.framework.Error

data class LineError(
    val text: String = ""
) : Error {
    override fun toString(): String = text
}
