package nous.application.command

import nous.domain.core.*
import nous.domain.jobs.*
import nous.utils.io.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import nous.utils.math.numberOfHands

object RunCommand: CliktCommand(help="Run a job") {
    private val job by argument().choice("filter", "extract")
    private val input by argument().path(mustExist = true)
    private val output by argument().path()
    private val source by option("-s", "--source").default("poker888")
    private val target by option("-t", "--target").default("poker888")

    override fun run() {
        val inp = input.toString()
        val out = output.toString()
        val src = Room(source)
        val trg = Room(target)
        val filter = Filter(src, trg)
        val extract = Extract(src, trg)
        val n = inp.numberOfHands()

        val progress = buildProgress("Parse", n)

        when(job) {
            "filter" -> filter(inp, out, progress)
            "extract" -> extract(inp, out, progress)
        }


    }
}
