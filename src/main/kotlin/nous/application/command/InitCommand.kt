package nous.application.command

import nous.utils.io.*
import com.github.ajalt.clikt.core.*
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import kotlin.io.path.*

object InitCommand : CliktCommand(help = "Initialize the pipeline") {
    private val input by argument("input").path(mustExist = true).default(Path("."))
    private val confirm by option("-y", "--yes").flag()

    override fun run() {
        val inp = sys("config")
        val out = Path(input.toString(),".nous")
        if(out.exists()) {
            if(!confirm) {
                val confirm = prompt("Overwrite the existing pipeline configuration? (Y/n)", "n")
                if (confirm == "Y") {
                    rm(out)
                    copy(inp, out)
                }
            } else {
                rm(out)
                copy(inp, out)
            }
            echo("Pipeline configuration was successfully updated.")
        } else {
            copy(inp, out)
            echo("Pipeline configuration was successfully created.")
        }
    }
}
