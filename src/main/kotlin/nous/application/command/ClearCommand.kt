package nous.application.command

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import nous.utils.io.*
import kotlin.io.path.exists
import kotlin.io.path.Path

object ClearCommand : CliktCommand(help = "Remove the pipeline configuration") {
    private val input by argument().path().default(Path("."))
    private val confirm by option("-y", "--yes").flag()

    override fun run() {
        val inp = Path(input.toString(), ".nous")
        if(inp.exists()) {
            if (!confirm) {
                val y = prompt("Are you sure to remove the existing configuration? (Y/n)", "n")
                if (y == "Y") {
                    rm(inp)
                }
            }
            echo("The configuration ${input.fileName} was successfully removed.")
        } else {
            echo("The configuration ${input.fileName} was not found.")
        }
    }
}
