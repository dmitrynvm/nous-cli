package nous.application.command

import nous.utils.fmt.*
import nous.utils.math.*
import com.github.ajalt.clikt.core.CliktCommand
import oshi.SystemInfo
import java.io.File
import java.time.LocalDate

object InfoCommand: CliktCommand(help="Operating system information") {
    override fun run() {
        val si = SystemInfo()
        val hw = si.hardware
        val cores = hw.processor.logicalProcessorCount
        val ramFree = measure(hw.memory.available)
        val ramTotal = measure(hw.memory.total)
        val hddFree = measure(File("/").freeSpace)
        val hddTotal = measure(hw.diskStores.sumOf { it.size })

        val os = "${si.operatingSystem.family} / amd${si.operatingSystem.bitness}"
        val java = System.getProperty("java.version")
        val built = LocalDate.now().toString()
        val cpu = "${hw.processor.maxFreq.div(10000000000.0)} MHz x $cores cores"
        val ram = "$ramFree / $ramTotal"
        val hdd = "$hddFree / $hddTotal"

        val m = arrayOf(
            arrayOf("PARAM", "VALUE"),
            arrayOf("Version", "0.1.12"),
            arrayOf("Java", java),
            arrayOf("Built", built),
            arrayOf("System / Arch", os),
            arrayOf("Processor", cpu),
            arrayOf("Memory", ram),
            arrayOf("Volume", hdd)
        )
        echo(m.toTable())
    }
}
