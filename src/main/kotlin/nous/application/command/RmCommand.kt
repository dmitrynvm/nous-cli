package nous.application.command

import nous.utils.io.*
import nous.utils.hash.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.types.*
import kotlin.io.path.exists

object RmCommand : CliktCommand(help = "Remove the image") {
    private val input by argument().path()
    private val confirm by option("-y", "--yes").flag()

    override fun run() {
        if(input.exists()) {
            if (!confirm) {
                val comfirm = prompt("Are you sure to remove the existing image? (Y/n)", "n")
                if (comfirm == "Y") {
                    rm(input)
                }
            }
            echo("The image ${input.fileName} was successfully removed.")
        } else {
            if (!confirm) {
                val comfirm = prompt("Are you sure to remove the existing image? (Y/n)", "n")
                val src = cur().dirs().first { hashid(it.name) == input.toString() }
                if (comfirm == "Y") {
                    rm(src)
                }
                echo("The image ${src.toPath().fileName} was successfully removed.")
            }
        }
    }
}
