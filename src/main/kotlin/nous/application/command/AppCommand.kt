package nous.application.command

import com.github.ajalt.clikt.core.CliktCommand

object AppCommand: CliktCommand(
    name = "nous",
    epilog = "Poker is a skill game pretending to be a chance game (James Altucher)"
) {
    override fun run() = Unit
}

