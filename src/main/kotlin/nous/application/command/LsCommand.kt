package nous.application.command

import nous.utils.io.*
import nous.utils.fmt.*
import nous.utils.hash.*
import nous.utils.math.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.types.*
import kotlin.io.path.*
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.time.LocalDateTime
import java.time.*

object LsCommand : CliktCommand(help = "List of images") {
    private val input by argument().path(mustExist = true).default(Path("."))

    override fun run() {
        val head = arrayOf("IMAGE", "TAG", "ID", "CREATED", "FILES", "SIZE")
        val body = input.dirs().map{
            val raw = Files.readAttributes(it.toPath(), BasicFileAttributes::class.java).lastModifiedTime()
            val start = LocalDateTime.ofInstant(raw.toInstant(), ZoneId.systemDefault())
            val stop = LocalDateTime.now()
            val duration = Duration.between(start, stop)
            val age = "${age(duration)} ago"
            val name = it.name.split("-").first()
            val tag = if("-" in it.name) it.name.split("-").last() else "latest"
            val id = hashid(it.name)
            arrayOf(name, tag, id, age, it.files().size.toString(), it.size())
        }.toTypedArray()
        echo(arrayOf(head, *body).toTable())
    }
}
