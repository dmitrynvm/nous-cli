package nous.application.command

import nous.utils.hash.*
import nous.utils.fmt.*
import nous.utils.io.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.*
import com.github.ajalt.clikt.parameters.types.*
import kotlin.io.path.exists

object DescribeCommand : CliktCommand(help = "Describe the image") {
    private val input by argument().path()

    override fun run() {
        val src = if(input.exists())
            input
        else
            pwd().dirs().first { hashid(it.name) == input.toString() }.toPath()

        val symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\t\n".toCharArray()
        val unicode = src
            .files()
            .fold(emptySet<Char>()) { acc, el -> acc.union(el.read().toSet()) }
            .let { it.filter { s -> !s.isDigit() && s !in symbols } }
            .sorted()
            .toString()
        val hash = src.files("txt").sumOf { it.length() }.toString()

        val body = arrayOf(
            arrayOf("source", src.toString()),
            arrayOf("symbols", unicode),
            arrayOf("digest", hash.toSha256())
        )
        echo(body.toTable())
    }
}
