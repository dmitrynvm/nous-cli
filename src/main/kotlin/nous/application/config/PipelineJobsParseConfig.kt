package nous.application.config

import nous.utils.fmt.*

data class PipelineJobsParseConfig(
    val name: String = "",
    val description: String = "",
) {
    fun toMap(): Map<String, Any> = toNode()
}
