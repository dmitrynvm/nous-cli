package nous.application.config

import nous.utils.fmt.*

data class PipelineConfig(
    val name: String  = "",
    val author: String = "",
    val description: String = "",
    val version: String = "",
    val env: PipelineEnvConfig = PipelineEnvConfig(),
    val jobs: PipelineJobsConfig = PipelineJobsConfig()
) {

    fun toMap(): Map<String, Any> = toNode()

//    import org.koin.core.component.*
//    import org.koin.core.context.*
//    import org.koin.dsl.module
//    import java.nio.file.Path
//    companion object : KoinComponent {
//
//        val basepath by inject<Path>()
//        val pipeline by lazy { yaml<PipelineConfig>(basepath) }
//
//        init {
//            startKoin {
//                modules(up())
//            }
//        }
//
//        private fun up() = module {
//            println(work("pipeline.yml").open().read())
//            single {
////            yaml<Pipeline>(work("pipeline.yml"))
//                work("pipeline.yml")
//            }
//        }
//
//        override fun toString(): String =
//            pipeline.toMap().toJson()
//
//    }
}
