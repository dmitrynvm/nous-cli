package nous.application.config

import nous.utils.fmt.*

data class PipelineJobsConfig(
    val parse: PipelineJobsParseConfig = PipelineJobsParseConfig(),
    val check: PipelineJobsCheckConfig = PipelineJobsCheckConfig(),
) {
    fun toMap(): Map<String, Any> = toNode()
}
