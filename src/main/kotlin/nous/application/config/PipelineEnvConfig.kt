package nous.application.config

import nous.utils.fmt.*

data class PipelineEnvConfig(
    val source: String = "",
    val target: String = ""
) {
    fun toMap(): Map<String, Any> = toNode()
}
