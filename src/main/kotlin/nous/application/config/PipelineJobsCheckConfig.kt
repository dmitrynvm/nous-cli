package nous.application.config

import nous.utils.fmt.*

data class PipelineJobsCheckConfig(
    val name: String = "",
    val description: String = "",
) {
    fun toMap(): Map<String, Any> = toNode()
}
