package nous.application.config

import nous.utils.fmt.*

data class ParseConfig(
    val name: String  = "",
    val author: String = "",
    val description: String = "",

    val env: ParseEnvConfig = ParseEnvConfig(),
    val pattern: ParsePatternConfig = ParsePatternConfig()
) {
    fun toMap(): Map<String, Any> = toNode()
}