package nous.application

import nous.application.command.*
import com.github.ajalt.clikt.core.subcommands

fun main(args: Array<String>) =
    AppCommand.subcommands(
        ClearCommand,
        InitCommand,
        DescribeCommand,
        LsCommand,
        RmCommand,
        RunCommand,
        InfoCommand
    ).main(args)
